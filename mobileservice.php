<?php
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    require './vendor/autoload.php';

    // $app = new \Slim\App;
    $app = new \Slim\App([
        'settings' => [
          'displayErrorDetails' => true
        ]
    ]);

    $app->get('/hi/{name}', function (Request $request, Response $response) {
		$name = $request->getAttribute('name');
		$response->getBody()->write("Hi, $name");
		// return $response;
    });

    $app->post('/header', function (Request $request, Response $response) {
        $head = $request->getHeader('File-Name');
        $data = array('head' => $head);
        $response = $response->withJson($data);
		return $response;
    });

    //======================== get server time ========================
    $app->get('/now', function (Request $request, Response $response) {
        //current time in the number of seconds since January 1 1970
		$data = array('time' => time());
        $response = $response->withJson($data);
		return $response;
    });

    //======================== gen password from string ========================
    $app->get('/password/{text}', function (Request $request, Response $response) {
        $text = $request->getAttribute('text');
        $pass = password_hash($text, PASSWORD_DEFAULT);
        $data['password'] = $pass;
        $data['length'] = strlen($pass);
        $response = $response->withJson($data);
		return $response;
    });

    $app->get('/password_check/{pass}', function (Request $request, Response $response) {
        $pass = $request->getAttribute('pass');
        $hash = '$2y$10$6sdA4ZTiFsh7jO1ey66vlORe8Mk.Vgay7Cgmyp8ivoUIzI7iSLQMu';
        $data['result'] = password_verify($pass, $hash);
        $response = $response->withJson($data);
		return $response;
    });

    ///=========================== get all users ========================
    $app->get('/all', function (Request $request, Response $response) {
		require_once('./plugin/connectDB.php');
		$query = "SELECT * FROM MFHA_USER";

		$stmt = $conn->stmt_init();
		//is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->execute();

			//check number of rows, should be > 0
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

			if($numrows > 0) {
				while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
				$response = $response->withJson($data);
                //echo json_encode($data);
			}
            else {
                // no data or wrong query
                $data = array('error_text' => 'No data');
                $response = $response->withStatus(204)->withJson($data);
            }

            $stmt->close();
            $result->free();
        }
		else {
			$response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

    //=========================== login ========================
    $app->post('/login', function (Request $request, Response $response) {
        //get post data, MUST NOT use variables $username, $password
        $username = $request->getParsedBody()['username'];
        $password = $request->getParsedBody()['password'];

		require_once('./plugin/connectDB.php');
		//only correct login info and active
        $query = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.ROLE, U.FIRST_NAME, U.LAST_NAME, U.ACADEMIC_YEAR, U.MAJOR_ID, U.QR_CODE, M.NAME AS MAJOR_NAME, S.NAME AS SCHOOL_NAME
         FROM MFHA_USER U JOIN MFHA_MAJOR_DEPARTMENT M ON U.MAJOR_ID=M.ID 
         JOIN MFHA_SCHOOL S ON M.SCHOOL_ID=S.ID 
         WHERE U.USERNAME=? AND U.ACTIVE_FLAG=1 AND (U.ROLE=0 or U.ROLE=1)";

        $stmt = $conn->stmt_init();

        //is query OK?
        if (($stmt = $conn->prepare($query)) == FALSE) {
            //query error
            $data = ['status' => 'Wrong Query'];
            $response = $response->withStatus(400)->withJson($data);
            return $response;
        }

        //query OK, bind and execute it
        $stmt->bind_param("s", $username);
        $stmt->execute();

        //check number of rows, should be only 1
        $result = $stmt->get_result();
        $numrows = $result->num_rows;
        //fetch result
        $data = $result->fetch_assoc();

        //free resources
        $result->free();
        $stmt->close();
        $conn->close();
        
        //no record, wrong login
        if($numrows != 1) {
            $data = ['status' => 'Wrong username'];
            $response = $response->withStatus(401)->withJson($data);
            return $response;
        }
  
        //verify hash password
        if(!password_verify($password, $data['PASSWORD'])) {
            //password does not match
            $data = ['status' => 'Wrong username or password'];
            $response = $response->withStatus(401)->withJson($data);
            return $response;
        }

        //correct login
        //remove password field, column 3
        array_splice($data, 2, 1);
        $response = $response->withJson($data);
        return $response;
    });

    ///=========================== get setting from key ========================
    $app->get('/setting/{key}', function (Request $request, Response $response) {
        $key = $request->getAttribute('key');

		require_once('./plugin/connectDB.php');
        $query = "SELECT VALUE FROM MFHA_SETTING WHERE SETTING_KEY=?";

        $stmt = $conn->stmt_init();

        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("s", $key);
            $stmt->execute();

            //check number of rows, should be only 1
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows == 1) {
                $data = $result->fetch_assoc();
                $response = $response->withJson($data);
            }
            else {
                $data = array('error_text' => 'No this setting value');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}
        $conn->close();
        return $response;
    });

    //=========================== get all activities by year ========================
    $app->get('/activity_year/{year}', function (Request $request, Response $response) {
        $year = $request->getAttribute('year');
		require_once('./plugin/connectDB.php');
		$query = "SELECT ID, TITLE, SCANNING_STARTDATETIME, SCANNING_ENDDATETIME
         FROM MFHA_ACTIVITY
         WHERE ACADEMIC_YEAR=?
         ORDER BY SCANNING_STARTDATETIME";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("i", $year);
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //activity retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no activity
                $data = ['status' => 'No activity'];
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//=========================== get today activity by year ========================
    $app->get('/activity_now/{year}', function (Request $request, Response $response) {
        $year = $request->getAttribute('year');
        require_once('./plugin/connectDB.php');
        
        //FIXME: the server's time may be in other regions, try to check it first and make it GMT+7
		$query = "SELECT ID, TITLE, SCANNING_STARTDATETIME, SCANNING_ENDDATETIME
         FROM MFHA_ACTIVITY
         WHERE ACADEMIC_YEAR=? AND NOW() >= SCANNING_STARTDATETIME  AND NOW() <= SCANNING_ENDDATETIME
         ORDER BY SCANNING_STARTDATETIME";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("i", $year);
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //activity retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no activity
                $data = ['status' => 'No Activity Today'];
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//================== get activity by userID and year ========================
    $app->get('/activity_user', function (Request $request, Response $response) {
		$userID = $request->getQueryParam('userID');
        $year = $request->getQueryParam('year');

		require_once('./plugin/connectDB.php');
		$query = "SELECT DISTINCT A.*, SA.STATUS_ACTIVITY FROM MFHA_ACTIVITY A LEFT JOIN MFHA_STUDENT_ACTIVITY SA ON A.ID=SA.ACTIVITY_ID AND SA.USER_ID=? WHERE A.ACADEMIC_YEAR=? ORDER BY A.SCANNING_STARTDATETIME";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("ii", $userID, $year);
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //activity retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no activity
                $data = array('status' => 'No activity');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
            //query error
            $data = array('status' => 'Query error', 'sql' => $query);
			$response = $response->withStatus(400)->withJson($data);;
		}

        $conn->close();
        return $response;
    });

	//=================== get major and school from major ID========================
    $app->get('/major_school/{majorID}', function (Request $request, Response $response) {
        $majorID = $request->getAttribute('majorID');

		require_once('./plugin/connectDB.php');
		//only correct login info and active
        $query = "SELECT MFHA_MAJOR_DEPARTMENT.MAJOR_NAME, MFHA_SCHOOL.SCHOOL_NAME
         FROM MFHA_MAJOR_DEPARTMENT
         JOIN MFHA_SCHOOL ON MFHA_MAJOR_DEPARTMENT.SCHOOL_ID=MFHA_SCHOOL.SCHOOL_ID
         WHERE MAJOR_ID = ?";

        $stmt = $conn->stmt_init();

        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("i", $majorID);
            $stmt->execute();

            //check number of rows, should be only 1
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows == 1) {
                $data = $result->fetch_assoc();
                $response = $response->withJson($data);
            }
            else {
                $data = array('error_text' => 'No major found');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}
        $conn->close();
        return $response;
    });

	//=================== get survey questions from activityID ========================
    $app->get('/survey_question/{activityID}', function (Request $request, Response $response) {
		$activityID = $request->getAttribute('activityID');
		require_once('./plugin/connectDB.php');
		$query = "SELECT * FROM MFHA_SURVEY_QUESTION
         WHERE ACTIVITY_ID=?";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("i", $activityID);
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //activity retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no activity
                $data = array('error_text' => 'No activity');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//=========================== save survey answers ========================
	$app->post('/survey_answer', function (Request $request, Response $response) {
        $user_id = $request->getParsedBody()['user_id'];
		$answer_values1 = $request->getParsedBody()['answer_values1'];
		$question_ID1 = $request->getParsedBody()['question_ID1'];		
		$answer_values2 = $request->getParsedBody()['answer_values2'];
		$question_ID2 = $request->getParsedBody()['question_ID2'];

		require_once('./plugin/connectDB.php');
		$query = "INSERT INTO MFHA_SURVEY_ANSWER (ANSWER, QUESTION_ID, USER_ID)
         VALUES (?,?,?), (?,?,?)";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->bind_param("siisii", $answer_values1, $question_ID1, $user_id, $answer_values2, $question_ID2, $user_id);
            $stmt->execute();

            //check number of inserted rows, should be positive
            $numrows = $stmt->affected_rows;
            $data = array('status' => 'Insertion completed');
            $response = $response->withJson($data);

            if($numrows <= 0) {
                //inserted failed
                $data = array('status' => 'Insertion failed');
                $response = $response->withStatus(401)->withJson($data);
            }
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//=========================== update activity status of student after survey ========================
	$app->put('/update_activity_status/{user_id}', function (Request $request, Response $response) {
		$user_id = $request->getAttribute('user_id');
		$activity_id = $request->getParsedBody()['activity_id'];
		$status_surveyed = 2;

		require_once('./plugin/connectDB.php');
		$query = "UPDATE MFHA_STUDENT_ACTIVITY A
         SET A.STATUS=?
         WHERE A.USER_ID=? AND A.ACTIVITY_ID=?";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("iii", $status_surveyed, $user_id, $activity_id);
            $stmt->execute();

            //check number of inserted rows, should be only 1
            $numrows = $stmt->affected_rows;

            if($numrows != 1) {
                //inserted failed
                $data = array('error_text' => 'Updated failed');
                $response = $response->withStatus(401)->withJson($data);
            }
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//=========================== get handbook list ========================
    $app->get('/handbook', function (Request $request, Response $response) {
		require_once('./plugin/connectDB.php');
		$query = "SELECT TITLE, SUBTITLE, FILE_NAME
         FROM MFHA_HANDBOOK
         WHERE ACTIVE_FLAG=1";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //handbook retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withStatus(200)->withJson($data);
            }
            else {
                //no handbook
                $data = array('error_text' => 'No handbook');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

	//====================== get an active single user info =====================
    $app->get('/user/{username}', function (Request $request, Response $response) {
        $username = $request->getAttribute('username');

		require_once('./plugin/connectDB.php');
		//only correct login info and active
        $query = "SELECT ID, FIRST_NAME
         FROM MFHA_USER
         WHERE USERNAME=? AND ACTIVE_FLAG=1";

        $stmt = $conn->stmt_init();

        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("s", $username);
            $stmt->execute();

            //check number of rows, should be only 1
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows == 1) {
                // find this user
                $data = $result->fetch_assoc();
                $response = $response->withStatus(200)->withJson($data);
            }
            else {
                // user not found
                $data = array('error_text' => 'Wrong username or password');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}
        $conn->close();
        return $response;
    });

	//======== get an active single user info for MFU talent application =======
    //====== user must exist, active and have not already registerd for MFU talent
    //====== the setting for registration must be ON (1)
    //FIXME: allow only freshmen
    $app->get('/canRegister/{username}', function (Request $request, Response $response) {
        $username = $request->getAttribute('username');

		require_once('./plugin/connectDB.php');
		//only an existing student who hasn't applied yet and registration setting is ON
        $query = "SELECT U.ID FROM MFHA_USER U, MFHA_SETTING S
         WHERE S.SETTING_KEY='registerGottalent' AND S.VALUE=1 AND
         U.USERNAME=? AND U.ROLE=0 AND U.ACTIVE_FLAG=1 AND U.ID NOT IN
         (SELECT G.USER_ID FROM MFHA_GOTTALENT_MEMBER G)";

        $stmt = $conn->stmt_init();

        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("s", $username);
            $stmt->execute();

            //check number of rows, should be only 1
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows == 1) {
                // find this user
                $data = $result->fetch_assoc();
                $response = $response->withJson($data);
            }
            else {
                // user not found
                $data = array('error_text' => 'Cannot register now or User is invalid or already registered');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}
        $conn->close();
        return $response;
    });

    //======== check whether a user can vote for MFU talent =======
    //====== user must exist, active and have not already voted for MFU talent
    //====== the setting for voting must be ON (1)
    //====== check user's survey >= 80%
    //TODO: test this service with mobile
    $app->get('/canVote', function (Request $request, Response $response) {
        $userID = $request->getQueryParam('userID');
        $year = $request->getQueryParam('year');
        $minRatio = 80;

        require_once('./plugin/connectDB.php');
        
        //--- CONDITION 1 ---
        //only an existing student who hasn't voted yet and vote setting is ON
        $query = "SELECT U.USERNAME FROM MFHA_USER U, MFHA_SETTING S
         WHERE S.SETTING_KEY='voteGottalent' AND S.VALUE=1 AND
         U.ID=? AND U.ROLE=0 AND U.ACTIVE_FLAG=1 AND U.VOTED IS NULL";
        $stmt = $conn->stmt_init();
        if(($stmt = $conn->prepare($query)) == FALSE) {
            $data['sql'] = $query;
            $response = $response->withStatus(400)->withJson($data);
            return $response;
        }
        $stmt->bind_param("i", $userID);
        $stmt->execute();
        $result = $stmt->get_result();
        $numrows = $result->num_rows;
        if($numrows != 1) {
            $data['status'] = "Cannot vote now or already voted";
            $response = $response->withStatus(401)->withJson($data);
            return $response;
        }

        //--- CONDITION 2 ---
        // find 80% survey ratio
        //total user's survey done
        //FIXME: just only HLL activities
        $query = "SELECT COUNT(ACTIVITY_ID) AS totalSurvey FROM MFHA_STUDENT_ACTIVITY WHERE USER_ID=? AND STATUS_ACTIVITY=2";
        if(($stmt = $conn->prepare($query)) == FALSE) {
            $data['sql'] = $query;
            $response = $response->withStatus(400)->withJson($data);
            return $response;
        }
        $stmt->bind_param("i", $userID);
        $stmt->execute();
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        $total_survey = $data['totalSurvey'];

        //total number of activities
        //FIXME: just only HLL activities
        $query = "SELECT COUNT(ID) AS totalActivity FROM MFHA_ACTIVITY WHERE ACADEMIC_YEAR=?";
        if(($stmt = $conn->prepare($query)) == FALSE) {
            $data['sql'] = $query;
            $response = $response->withStatus(400)->withJson($data);
            return $response;
        }
        $stmt->bind_param("i", $year);
        $stmt->execute();
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        $total_activity = $data['totalActivity'];

        $ratio = $total_survey / $total_activity *100;
        $data = ["status" => "Reaches 80%, you can vote"];
        $response = $response->withJson($data);
        if($ratio < $minRatio) {
            $data = ["status" => "Sorry, please complete more survey to vote"];
            $response = $response->withStatus(401)->withJson($data);
        }
        
        $stmt->close();
        $conn->close();
        return $response;
    });

    //================= Upload talent image without form =========================
    $app->post('/uploadImageTalent', function(Request $request, Response $response) {
        //TODO: check with web party
        //where to save uploading image
        $directory = "./img";
        //get image filename from header
        $name = $request->getHeader("File-Name");
        //combine folder timestamp and filename
        // $path = $directory.DIRECTORY_SEPARATOR.time()."_".$name[0];
        $path = $directory.DIRECTORY_SEPARATOR.$name[0];
        //get file content from body
        $data = $request->getBody()->getContents();
        //save file to defined location
        file_put_contents($path, $data);
        //return response if necessary
        // $message['content'] = $contentType;
        // $message['name'] = $name;
        // return $response->withJson($message);
    });

    //=========================== register talent team+member info ========================
	$app->post('/registerTalent', function (Request $request, Response $response) {
        //FIXME: Rewrite this service for better structure, see: /canVote
		$perfString = $request->getParsedBody()['perfString'];
        $memberString = $request->getParsedBody()['memberString'];
        $photoFilename = $request->getParsedBody()['photoFilename'];
        $year = $request->getParsedBody()['year'];
        
        //convert string to JSON array
        //performance: teamTitle, perfTitle, perfType
        $arrPerf = json_decode($perfString, true);
        //member: [id, username, nickname, tel, facebook]
        $arrMember = json_decode($memberString, true);

        require_once('./plugin/connectDB.php');
        $query = "INSERT INTO MFHA_GOTTALENT_FORM (TEAM_TITLE, PERFORMANCE_TITLE, STUDENT_PICTURE, NUMBER_MEMBER, ACADEMIC_YEAR)
         VALUES (?,?,?,?,?)";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
            //number of members
            $num_member = count($arrMember);

			$stmt->bind_param("sssii", $arrPerf["teamTitle"], $arrPerf["perfTitle"], $photoFilename, $num_member, $year);
            $stmt->execute();

            //check number of inserted rows, should be only 1
            $numrows = $stmt->affected_rows;
            $data['status'] = "Registration complete";            
            $response = $response->withJson($data);         

            if($numrows != 1) {
                //inserted failed
                $data['status'] = "Team Registration failed";
                $response = $response->withStatus(401)->withJson($data);
            }
            else {                
                //get last insert ID
                $lastID = $stmt->insert_id;

                //insert each member info
                $query = "INSERT INTO MFHA_GOTTALENT_MEMBER (USER_ID, NICKNAME, PHONE_NUMBER, FACEBOOK, GOTTALENTFORM_ID) VALUES ";
                for($i=0;$i<count($arrMember);$i++) {                   
                    $query = $query."('".$arrMember[$i]['id']."','".$arrMember[$i]['nickname']."','".$arrMember[$i]['tel']."','".$arrMember[$i]['facebook']."','".$lastID."')";
                    
                    if($i != count($arrMember)-1) {
                        $query = $query.",";
                    }
                }

                // $data['status'] = $query;          
                // $response = $response->withJson($data); 

                if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $numrows = $stmt->affected_rows;
                    if($numrows < 1) {
                        //inserted failed
                        $data['status'] = "Member Registration failed";
                        $response = $response->withStatus(401)->withJson($data);
                    }
                }
                else {
                    //query error
                    $data['status'] = "Query error";
                    $response = $response->withStatus(400)->withJson($data);
                }
            }            
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $stmt->close();
        $conn->close();
        return $response;
    });

    //=========================== get talent team list ========================
    $app->get('/talent_team/{year}', function (Request $request, Response $response) {
        $year = $request->getAttribute('year');

		require_once('./plugin/connectDB.php');
		$query = "SELECT ID, TEAM_TITLE, TEAM_NUMBER, PERFORMANCE_TITLE, AEDO_PICTURE, VOTE_PICTURE
         FROM MFHA_GOTTALENT_FORM
         WHERE PASS_FLAG=1 AND ACADEMIC_YEAR=?
         ORDER BY TEAM_NUMBER";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->bind_param("s", $year);
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //team retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no team
                $data = array('error_text' => 'No team');
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

    //=========================== vote and update result to DB ========================
    $app->put('/voteTalent', function (Request $request, Response $response) {
		$teamID = $request->getParsedBody()['teamID'];
		$userID = $request->getParsedBody()['userID'];

		require_once('./plugin/connectDB.php');
		$query = "UPDATE MFHA_GOTTALENT_FORM
         SET VOTE_POINT = VOTE_POINT+1
         WHERE ID=?";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
			$stmt->bind_param("i", $teamID);
            $stmt->execute();

            //check number of inserted rows, should be only 1
            $numrows = $stmt->affected_rows;

            $data['status'] = "Vote complete";
            $response = $response->withJson($data);

            if($numrows != 1) {
                //inserted failed
                $data['status'] = "Vote failed";
                $response = $response->withStatus(401)->withJson($data);
                return $response;
            }

            //after updating vote score, update user's vote status
            $query = "UPDATE mfha_user
             SET VOTED=1
             WHERE ID=?";

            if ($stmt = $conn->prepare($query)) {
                $stmt->bind_param("i", $userID);
                $stmt->execute();

                //check number of inserted rows, should be only 1
                $numrows = $stmt->affected_rows;

                $data['status'] = "All vote complete";
                $response = $response->withJson($data);

                if($numrows != 1) {
                    //inserted failed
                    $data['status'] = "All vote failed";
                    $response = $response->withStatus(401)->withJson($data);
                }
            }     
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $stmt->close();
        $conn->close();
        return $response;
    });

    //=========================== get contact ========================
    $app->get('/contact', function (Request $request, Response $response) {
		require_once('./plugin/connectDB.php');
		$query = "SELECT TITLE, PICTURE, URL
         FROM MFHA_CONTACT
         WHERE ACTIVE_FLAG=1
         ORDER BY UPLOADED_DATE";

        $stmt = $conn->stmt_init();
        //is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->execute();

            //check number of rows, should be positive
            $result = $stmt->get_result();
            $numrows = $result->num_rows;

            if($numrows > 0) {
                //contact retrieved
                while($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
                $response = $response->withJson($data);
            }
            else {
                //no contact
                $data['status'] = "No team";
                $response = $response->withStatus(401)->withJson($data);
            }

            $result->free();
            $stmt->close();
        }
		else {
			//query error
			$response = $response->withStatus(400);
		}

        $conn->close();
        return $response;
    });

    //=========================== get news list ========================
    $app->get('/news', function (Request $request, Response $response) {
        $number = $request->getQueryParam('number');
        //get only 'number' of news, if number is null, get all news

		require_once('./plugin/connectDB.php');
		$query = "SELECT TITLE, SUBTITLE, HEADER_PICTURE, DETAIL_PICTURE, URL_LINK
         FROM MFHA_NEWS
         WHERE START_DATETIME <= NOW() AND END_DATETIME >= NOW()
         ORDER BY START_DATETIME DESC";

        if($number > 0) {
            $query = $query." LIMIT $number";
        }

        $stmt = $conn->stmt_init();
        //is query OK?
        if (($stmt = $conn->prepare($query)) == FALSE) {
            //query error
            $data = ['status' => 'Wrong Query'];
            $response = $response->withStatus(400)->withJson($data);
            $stmt->close();
            $conn->close();
            return $response;
        }

        //query OK, bind and execute it
        $stmt->execute();
        
        //check number of rows, should be positive
        $result = $stmt->get_result();
        $numrows = $result->num_rows;

        //no news
        if($numrows <= 0) {
            $data = ['status' => 'Wrong Query'];
            $response = $response->withStatus(401)->withJson($data);
            $result->free();
            $stmt->close();
            $conn->close();
            return $response;
        }

        //found some news
        //fetch result
        while($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        $response = $response->withJson($data);
        $result->free();
        $stmt->close();
        $conn->close();
        return $response;
    });

    //=========================== save QR scan results ========================
	$app->post('/uploadQR', function (Request $request, Response $response) {
        $scanString = $request->getParsedBody()['scanString'];
        $scanJSON = json_decode($scanString, TRUE);

        require_once('./plugin/connectDB.php');

        //note that using the following sql requires unique key
        $query = "INSERT IGNORE INTO MFHA_STUDENT_ACTIVITY (USER_ID, ACTIVITY_ID, STATUS_ACTIVITY, SCAN_BY_ID, SCANNING_TIME) VALUES ";

        //attend status is 1
        $attendStatus = 1;

        for($i=0; $i<count($scanJSON); $i++) {
            $query = $query."(".$scanJSON[$i]['USER_ID'].",".$scanJSON[$i]['ACTIVITY_ID'].",$attendStatus,".$scanJSON[$i]['SCAN_BY_ID'].",'".$scanJSON[$i]['SCANNING_TIME']."')";
            //not the last term
            if($i<count($scanJSON)-1) {
                $query = $query.", ";
            }
        }        

        $stmt = $conn->stmt_init();
        // //is query OK?
        if ($stmt = $conn->prepare($query)) {
            $stmt->execute();

            //check number of inserted rows, should be positive
            $numrows = $stmt->affected_rows;
            $data = array('status' => 'Insertion completed with '.$numrows.' records');
            $response = $response->withJson($data);

            if($numrows <= 0) {
                //inserted failed
                $data = array('status' => 'Repeated scan data, inserted rows = '.$numrows);
                $response = $response->withStatus(401)->withJson($data);
            }
            $stmt->close();
        }
		else {
            //query error
            $data = array('status' => 'Query error', 'sql' => $query);
			$response = $response->withStatus(400)->withJson($data);
		}

        $conn->close();
        return $response;
    });

    //=========================== OBSOLETE: save QR scan results ========================
    /*
	$app->post('/scanQR', function (Request $request, Response $response) {
        $atvID =$request->getParsedBody()['atvID'];
        $staffID = $request->getParsedBody()['staffID'];
        $scanString = $request->getParsedBody()['scanString'];
        $scanJSON = json_decode($scanString, TRUE);

        require_once('./plugin/connectDB.php');
        //find better solution
        //CHOICE 1: insert non-repeated converted ID and then update timestamp
        // $query = "INSERT INTO mfha_student_activity(USER_ID, ACTIVITY_ID, STATUS, SCAN_BY_ID,SCANNING_TIME) SELECT ID, 4, 1, 5, '2018-03-18 13:11:45' FROM mfha_user WHERE NOT EXISTS (SELECT USER_ID FROM mfha_student_activity WHERE mfha_student_activity.USER_ID=mfha_user.ID) AND (mfha_user.USERNAME=6131305001 OR mfha_user.USERNAME=6131501001)";

        //note that using the following sql requires unique key
        // $query = "INSERT IGNORE INTO mfha_student_activity(USER_ID, ACTIVITY_ID, STATUS, SCAN_BY_ID, SCANNING_TIME)  SELECT U.ID, 4, 1, 5, '0000-00-00 00:00:00' FROM mfha_user U WHERE U.USERNAME=6131305001 OR U.USERNAME=6131501001";

        // $query = "INSERT IGNORE INTO mfha_student_activity(USER_ID, ACTIVITY_ID, STATUS, SCAN_BY_ID, SCANNING_TIME)
        //  SELECT U.ID, ?, 1, ?, '0000-00-00 00:00:00'
        //  FROM mfha_user
        //  WHERE ";

        // for($i=0; $i<count($scanJSON); $i++) {
        //     $query = $query."USERNAME=".$scanJSON[$i]['USERNAME'];
        //     if($i<count($scanJSON)-1) {
        //         $query = $query." OR ";
        //     }
        // }
        //----------------------------------------------------------------

        //CHOICE 2: create temp table, update its username to ID, insert non-repeated to target table
        $query = "CREATE TEMPORARY TABLE IF NOT EXISTS temp LIKE mfha_student_activity";
        

        $query = "INSERT INTO temp (USER_ID, ACTIVITY_ID, STATUS, SCAN_BY_ID, SCANNING_TIME) VALUES ";
        for($i=0; $i<count($scanJSON); $i++) {
            $query = $query."(".$scanJSON[$i]['USERNAME'].",".$atvID.",2,".$staffID.",".$scanJSON[$i]['TIME'].")";
            if($i<count($scanJSON)-1) {
                $query = $query.", ";
            }
        }
        
        $query = "UPDATE temp SET temp.USER_ID=mfha_user.ID WHERE mfha_user.USERNAME=temp.USER_ID";


        $query = "INSERT IGNORE INTO mfha_student_activity(USER_ID, ACTIVITY_ID, STATUS, SCAN_BY_ID, SCANNING_TIME) SELECT * FROM temp";

        $query = "DROP TABLE temp";


        // $data['sql'] = $query;
        // $response = $response->withJson($data);

        // $stmt = $conn->stmt_init();
        // //is query OK?
        // if ($stmt = $conn->prepare($query)) {
        //     $stmt->bind_param("isss", $scanJSON['ID'], $question_ID1, $user_id, $answer_values2, $question_ID2, $user_id);
        //     $stmt->execute();

        //     //check number of inserted rows, should be positive
        //     $numrows = $stmt->affected_rows;
        //     $data = array('status' => 'Insertion completed');
        //     $response = $response->withJson($data);

        //     if($numrows <= 0) {
        //         //inserted failed
        //         $data = array('status' => 'Insertion failed');
        //         $response = $response->withStatus(401)->withJson($data);
        //     }
        //     $stmt->close();
        // }
		// else {
		// 	//query error
		// 	$response = $response->withStatus(400);
		// }

        $conn->close();
        return $response;
    });
    */

    //============================================================================
    $app->run();
?>