-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 22, 2018 at 02:44 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mfha`
--
CREATE DATABASE IF NOT EXISTS `mfha` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mfha`;

-- --------------------------------------------------------

--
-- Table structure for table `mfha_activity`
--

CREATE TABLE `mfha_activity` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `QR_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `SCANNING_STARTDATETIME` datetime NOT NULL,
  `SCANNING_ENDDATETIME` datetime NOT NULL,
  `SURVEY_STARTDATETIME` datetime NOT NULL,
  `SURVEY_ENDDATETIME` datetime NOT NULL,
  `ACADEMIC_YEAR` int(11) NOT NULL,
  `CREATED_BY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_activity`
--

INSERT INTO `mfha_activity` (`ID`, `TITLE`, `QR_CODE`, `SCANNING_STARTDATETIME`, `SCANNING_ENDDATETIME`, `SURVEY_STARTDATETIME`, `SURVEY_ENDDATETIME`, `ACADEMIC_YEAR`, `CREATED_BY`) VALUES
(4, 'How to Learn', NULL, '2018-02-10 08:00:00', '2018-03-30 09:00:00', '2018-03-23 16:00:00', '2018-03-31 19:00:00', 2018, 3),
(5, 'The Inspiration', NULL, '2018-02-20 09:00:00', '2018-02-22 10:00:00', '2018-03-22 09:00:00', '2018-03-31 17:00:00', 2018, 3),
(6, 'Meet the Alumni', NULL, '2018-02-13 08:00:00', '2018-02-13 09:00:00', '2018-03-13 08:00:00', '2018-03-31 18:00:00', 2018, 3),
(7, 'Ha Hey Sports', '', '2018-04-04 00:00:00', '2018-04-04 23:00:00', '2018-03-04 00:00:00', '2018-05-30 23:00:00', 2018, 3),
(8, 'MFU Star Got Talent\'s 2018', '', '2018-05-08 00:00:00', '2018-05-31 23:00:00', '2018-04-13 00:00:00', '2018-04-13 23:00:00', 2018, 3),
(9, 'Orientation', '', '2018-05-08 00:00:00', '2018-05-31 23:00:00', '2018-03-30 00:00:00', '2018-04-30 23:00:00', 2018, 5);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_contact`
--

CREATE TABLE `mfha_contact` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE_FLAG` int(11) NOT NULL DEFAULT '0',
  `UPLOADED_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `UPLOADED_BY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_contact`
--

INSERT INTO `mfha_contact` (`ID`, `TITLE`, `PICTURE`, `URL`, `ACTIVE_FLAG`, `UPLOADED_DATE`, `UPLOADED_BY`) VALUES
(3, 'School of Information Technology', 'contact-ITschool.png', 'http://itschool.mfu.ac.th', 1, '2018-04-19 19:43:08', 8),
(4, NULL, 'Contact_Picture2', '', 0, '0000-00-00 00:00:00', 3),
(7, 'AEDO', 'contact-aedo.png', 'http://aedo.mfu.ac.th', 1, '2018-04-19 19:43:34', 8),
(8, NULL, 'ContactPicture04', '', 0, '0000-00-00 00:00:00', 8),
(9, NULL, 'PIFFFFFFFF', '', 0, '0000-00-00 00:00:00', 3),
(10, 'MFU Student Dormitory', 'contact-dormitory.png', '', 1, '2018-04-04 17:19:05', 3),
(11, 'Department of Student Affairs', 'student_affair.png', '', 0, '2018-03-23 15:09:26', 8);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_gottalent_form`
--

CREATE TABLE `mfha_gottalent_form` (
  `ID` int(11) NOT NULL,
  `TEAM_TITLE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TEAM_NUMBER` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PERFORMANCE_TITLE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `STUDENT_PICTURE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `AEDO_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VOTE_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASS_FLAG` int(11) DEFAULT NULL,
  `NUMBER_MEMBER` int(11) NOT NULL,
  `ACADEMIC_YEAR` int(11) NOT NULL,
  `VOTE_POINT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `mfha_gottalent_form`
--

INSERT INTO `mfha_gottalent_form` (`ID`, `TEAM_TITLE`, `TEAM_NUMBER`, `PERFORMANCE_TITLE`, `STUDENT_PICTURE`, `AEDO_PICTURE`, `VOTE_PICTURE`, `PASS_FLAG`, `NUMBER_MEMBER`, `ACADEMIC_YEAR`, `VOTE_POINT`) VALUES
(1, 'godzila', 'MTC01', 'CSDancing', 'Student_pic1', '2018_mtc01_thumb.jpg', '2018_mtc01.jpg', 1, 3, 2018, 2),
(2, 'MFES', '', 'mfessolution', 'Student_pic2', '', NULL, 0, 1, 2018, 0),
(3, 'Lamu', 'MTC03', 'MFU', 'Student_pic3', '2018_mtc03_thumb.jpg', '2018_mtc03.jpg', 1, 1, 2018, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_gottalent_member`
--

CREATE TABLE `mfha_gottalent_member` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `NICKNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PHONE_NUMBER` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `FACEBOOK` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `GOTTALENTFORM_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_gottalent_member`
--

INSERT INTO `mfha_gottalent_member` (`ID`, `USER_ID`, `NICKNAME`, `PHONE_NUMBER`, `FACEBOOK`, `GOTTALENTFORM_ID`) VALUES
(10, 2, 'studenNickname1', '0987654321', 'studentFacebook', 1),
(11, 1, 'stu1', '0812345678', 'stu_1', 1),
(16, 6, 'mfu', '0812345678', 'noe', 1),
(17, 13, 'ruttana', '0871234567', 'ruttana', 3),
(18, 14, 'joey', '0834951678', 'joey grabella', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_handbook`
--

CREATE TABLE `mfha_handbook` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SUBTITLE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `FILE_NAME` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE_FLAG` int(11) DEFAULT NULL,
  `UPLOADED_BY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_handbook`
--

INSERT INTO `mfha_handbook` (`ID`, `TITLE`, `SUBTITLE`, `FILE_NAME`, `ACTIVE_FLAG`, `UPLOADED_BY`) VALUES
(6, 'How to Live 2018', 'Manual, Agenda, and all student information of how to live', 'howtolive-2018.pdf', 1, 3),
(7, 'How to Learn 2018', 'Manual, Agenda, and all student information of how to learn', 'howtolearn-2018.pdf', 1, 3),
(8, 'How to Live 2017', '', 'howtolive-2017.pdf', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_major_department`
--

CREATE TABLE `mfha_major_department` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SCHOOL_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_major_department`
--

INSERT INTO `mfha_major_department` (`ID`, `NAME`, `SCHOOL_ID`) VALUES
(1, 'Computer Science and Innovation', 1),
(2, 'ADEO', 2),
(3, 'Software Engineering', 1),
(4, 'Computer Engineering', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_news`
--

CREATE TABLE `mfha_news` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBTITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HEADER_PICTURE` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DETAIL_PICTURE` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `URL_LINK` longtext COLLATE utf8_unicode_ci,
  `START_DATETIME` datetime NOT NULL,
  `END_DATETIME` datetime NOT NULL,
  `UPLOADED_BY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_news`
--

INSERT INTO `mfha_news` (`ID`, `TITLE`, `SUBTITLE`, `HEADER_PICTURE`, `DETAIL_PICTURE`, `URL_LINK`, `START_DATETIME`, `END_DATETIME`, `UPLOADED_BY`) VALUES
(2, 'Activities', 'Many fun activities are waiting for you', 'news4h.jpg', 'news4.jpg', 'https://www.google.com', '2018-02-06 00:00:00', '2018-05-24 00:00:00', 7),
(3, 'Talent Contest', 'Show your talent to the world', 'news2h.jpg', 'news2.jpg', 'https://www.bing.com', '2018-02-05 00:00:00', '2018-05-30 00:00:00', 7),
(4, 'description4', '', 'Header_Picture4', 'Detail_Picture4', 'url4', '2018-02-19 00:00:00', '2018-02-24 00:00:00', 14),
(6, 'Do and Don\'t', 'Please behave properly and show respect', 'news3h.jpg', 'news3.jpg', 'https://www.w3schools.com', '2018-02-15 00:00:00', '2018-05-21 00:00:00', 3),
(8, 'Orientation', 'Welcome all freshmen', 'news1h.jpg', 'news1.jpg', 'http://www.mfu.ac.th', '2018-03-23 00:00:00', '2018-05-30 00:00:00', 8);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_school`
--

CREATE TABLE `mfha_school` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_school`
--

INSERT INTO `mfha_school` (`ID`, `NAME`) VALUES
(1, 'School of Information Technology'),
(2, 'หน่วยงานของมหาวิทยาลัยเเม่ฟ้าหลวง');

-- --------------------------------------------------------

--
-- Table structure for table `mfha_setting`
--

CREATE TABLE `mfha_setting` (
  `ID` int(11) NOT NULL,
  `SETTING_KEY` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `VALUE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_setting`
--

INSERT INTO `mfha_setting` (`ID`, `SETTING_KEY`, `VALUE`) VALUES
(1, 'voteGottalent', 1),
(2, 'registerGottalent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_student_activity`
--

CREATE TABLE `mfha_student_activity` (
  `USER_ID` int(11) NOT NULL,
  `ACTIVITY_ID` int(11) NOT NULL,
  `STATUS_ACTIVITY` int(11) NOT NULL,
  `SCAN_BY_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SCANNING_TIME` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_student_activity`
--

INSERT INTO `mfha_student_activity` (`USER_ID`, `ACTIVITY_ID`, `STATUS_ACTIVITY`, `SCAN_BY_ID`, `SCANNING_TIME`) VALUES
(1, 4, 1, '5', '0000-00-00 00:00:00'),
(2, 4, 1, '5', '0000-00-00 00:00:00'),
(19, 4, 2, '5', '0000-00-00 00:00:00'),
(19, 5, 2, '5', '0000-00-00 00:00:00'),
(19, 6, 2, '5', '0000-00-00 00:00:00'),
(19, 7, 1, '5', '0000-00-00 00:00:00'),
(20, 9, 2, '5', '2018-05-06 13:55:31'),
(21, 9, 2, '5', '2018-05-06 13:55:31');

-- --------------------------------------------------------

--
-- Table structure for table `mfha_survey_answer`
--

CREATE TABLE `mfha_survey_answer` (
  `ID` int(11) NOT NULL,
  `ANSWER` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `QUESTION_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_survey_answer`
--

INSERT INTO `mfha_survey_answer` (`ID`, `ANSWER`, `QUESTION_ID`, `USER_ID`) VALUES
(1, '5', 1, 1),
(2, '4', 1, 2),
(4, 'good', 2, 1),
(5, '5', 1, 19),
(6, 'I love this', 3, 19),
(7, '3', 4, 19),
(8, 'Too much time', 5, 19),
(9, '1', 2, 19),
(10, 'Bad organization', 6, 19);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_survey_question`
--

CREATE TABLE `mfha_survey_question` (
  `ID` int(11) NOT NULL,
  `TEXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ORDER` int(11) NOT NULL,
  `ANSWER_TYPE` int(11) NOT NULL,
  `ACTIVITY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_survey_question`
--

INSERT INTO `mfha_survey_question` (`ID`, `TEXT`, `ORDER`, `ANSWER_TYPE`, `ACTIVITY_ID`) VALUES
(1, 'Please evaluate this activity', 0, 1, 4),
(2, 'Please evaluate this activity', 0, 1, 5),
(3, 'Please give your opinion', 0, 2, 4),
(4, 'Please evaluate this activity', 0, 1, 6),
(5, 'Please give your opinion', 0, 2, 6),
(6, 'Please give your opinion', 0, 2, 5),
(7, 'Please evaluate this activity', 0, 1, 7),
(8, 'Please give your opinion', 0, 2, 7),
(9, 'Please evaluate this activity', 1, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `mfha_user`
--

CREATE TABLE `mfha_user` (
  `ID` int(11) NOT NULL,
  `USERNAME` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ROLE` int(11) NOT NULL,
  `FIRST_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `LAST_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ACTIVE_FLAG` int(11) NOT NULL,
  `ACADEMIC_YEAR` int(11) DEFAULT NULL,
  `VOTED` int(11) DEFAULT NULL,
  `QR_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAJOR_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfha_user`
--

INSERT INTO `mfha_user` (`ID`, `USERNAME`, `PASSWORD`, `ROLE`, `FIRST_NAME`, `LAST_NAME`, `ACTIVE_FLAG`, `ACADEMIC_YEAR`, `VOTED`, `QR_CODE`, `MAJOR_ID`) VALUES
(1, '6031302014', '1111111111111', 0, 'Student1', 'Student1', 1, 2017, NULL, NULL, 1),
(2, '6131302014', '$2y$10$DJlEIM0.hXR6wOltDGA2RemUwDA9UKFRZojM0UoAkW.kNbdGS8oOG', 0, 'Student2', 'Student2', 1, 2018, NULL, NULL, 1),
(3, '123456', '1111111111111', 2, 'Admin1', 'Admin1', 1, NULL, NULL, NULL, 2),
(5, '5731302014', '$2y$10$V0cFO8m6eqynifGVYwroAezbKOqAixgRp88NjDa4tbKKXiHCz60u6', 1, 'Staff1', 'Staff1', 1, 2018, NULL, NULL, 1),
(6, '5831302014', '1111111111111', 0, 'Sanpetch', 'Boriboon', 0, 2015, NULL, NULL, 1),
(7, '5931302014', '1111111111111', 0, 'M', 'Apichat', 0, 2016, NULL, NULL, 1),
(8, '333333', '1111111111113', 2, 'admin', 'admin', 1, NULL, NULL, NULL, 2),
(9, '5931302020', '1111111111114', 1, 'staff', 'staff', 1, NULL, NULL, NULL, 1),
(10, '678342', '1111111111115', 2, 'admin2', 'admin', 1, NULL, NULL, NULL, 2),
(11, '658327', '1111111111116', 2, 'admin3', 'admin', 1, NULL, NULL, NULL, 2),
(12, '534876', '1111111111117', 2, 'admin4', 'admin', 1, NULL, NULL, NULL, 2),
(13, '5931273724', '1111111111118', 1, 'staff2', 'staff', 1, 2017, NULL, NULL, 1),
(14, '5931281364', '1111111111119', 1, 'staff3', 'staff', 0, 2017, NULL, NULL, 1),
(15, '5731241324', '1111111111120', 1, 'staff4', 'staff', 1, 2014, NULL, NULL, 1),
(16, '5831302022', '1111111111121', 1, 'staff5', 'staff', 1, 2015, NULL, NULL, 1),
(17, '5731302002', '1111111111122', 1, 'staff6', 'staff', 1, 2014, NULL, NULL, 1),
(18, '6031302003', '1111111111233', 0, 'supachai', 'student', 0, 2017, NULL, NULL, 1),
(19, '6131302001', '$2y$10$O5bIpANFVm/sSB8I.Tv65O56Zyx6G0r8zrk2dCpeHG6EhWln4qnhe', 0, 'Somchai', 'Deeprompt', 1, 2018, NULL, 'qr_6131302001.png', 1),
(20, '6131305001', '$2y$10$lfOY5GV0y..YZFGaBCoSb.9Dybzn6jcapJNXZWzsmpNnN0vjohVpe', 0, 'Nicha', 'Narak', 1, 2018, 1, 'qr_6131305001.png', 3),
(21, '6131501001', '$2y$10$t/aShxEW2ZY5ntS25f6HM.xsDXGFVcZmSrsumfEnDl229KK7iDwiq', 0, 'Khundesh', 'Peemeun', 1, 2018, NULL, 'qr_6131501001.png', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mfha_activity`
--
ALTER TABLE `mfha_activity`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Activity_User1_idx` (`CREATED_BY`);

--
-- Indexes for table `mfha_contact`
--
ALTER TABLE `mfha_contact`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Contact_User1_idx` (`UPLOADED_BY`);

--
-- Indexes for table `mfha_gottalent_form`
--
ALTER TABLE `mfha_gottalent_form`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mfha_gottalent_member`
--
ALTER TABLE `mfha_gottalent_member`
  ADD PRIMARY KEY (`ID`,`USER_ID`),
  ADD KEY `fk_Gotalent_member_Gotalent_frorm1_idx` (`GOTTALENTFORM_ID`),
  ADD KEY `fk_Gotalent_member_User1_idx` (`USER_ID`);

--
-- Indexes for table `mfha_handbook`
--
ALTER TABLE `mfha_handbook`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Handbook_User_idx` (`UPLOADED_BY`);

--
-- Indexes for table `mfha_major_department`
--
ALTER TABLE `mfha_major_department`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Major_Department_School1_idx` (`SCHOOL_ID`);

--
-- Indexes for table `mfha_news`
--
ALTER TABLE `mfha_news`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_News_User1_idx` (`UPLOADED_BY`);

--
-- Indexes for table `mfha_school`
--
ALTER TABLE `mfha_school`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mfha_setting`
--
ALTER TABLE `mfha_setting`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mfha_student_activity`
--
ALTER TABLE `mfha_student_activity`
  ADD PRIMARY KEY (`USER_ID`,`ACTIVITY_ID`),
  ADD KEY `fk_Student_Activity_User1_idx` (`USER_ID`),
  ADD KEY `fk_Student_Activity_Activity1_idx` (`ACTIVITY_ID`);

--
-- Indexes for table `mfha_survey_answer`
--
ALTER TABLE `mfha_survey_answer`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Survey_Answer_User1_idx` (`USER_ID`),
  ADD KEY `fk_Survey_Answer_Survey_Question1_idx` (`QUESTION_ID`);

--
-- Indexes for table `mfha_survey_question`
--
ALTER TABLE `mfha_survey_question`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Survey_Answer_copy1_Activity1_idx` (`ACTIVITY_ID`);

--
-- Indexes for table `mfha_user`
--
ALTER TABLE `mfha_user`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_User_Major_Department1_idx` (`MAJOR_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mfha_activity`
--
ALTER TABLE `mfha_activity`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mfha_contact`
--
ALTER TABLE `mfha_contact`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mfha_gottalent_form`
--
ALTER TABLE `mfha_gottalent_form`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mfha_gottalent_member`
--
ALTER TABLE `mfha_gottalent_member`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `mfha_handbook`
--
ALTER TABLE `mfha_handbook`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mfha_major_department`
--
ALTER TABLE `mfha_major_department`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mfha_news`
--
ALTER TABLE `mfha_news`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mfha_school`
--
ALTER TABLE `mfha_school`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mfha_setting`
--
ALTER TABLE `mfha_setting`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mfha_survey_answer`
--
ALTER TABLE `mfha_survey_answer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mfha_survey_question`
--
ALTER TABLE `mfha_survey_question`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mfha_user`
--
ALTER TABLE `mfha_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mfha_activity`
--
ALTER TABLE `mfha_activity`
  ADD CONSTRAINT `fk_Activity_User1` FOREIGN KEY (`CREATED_BY`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_contact`
--
ALTER TABLE `mfha_contact`
  ADD CONSTRAINT `fk_Contact_User1` FOREIGN KEY (`UPLOADED_BY`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_gottalent_member`
--
ALTER TABLE `mfha_gottalent_member`
  ADD CONSTRAINT `fk_Gotalent_member_Gotalent_frorm1` FOREIGN KEY (`GOTTALENTFORM_ID`) REFERENCES `mfha_gottalent_form` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Gotalent_member_User1` FOREIGN KEY (`USER_ID`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_handbook`
--
ALTER TABLE `mfha_handbook`
  ADD CONSTRAINT `fk_Handbook_User` FOREIGN KEY (`UPLOADED_BY`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_major_department`
--
ALTER TABLE `mfha_major_department`
  ADD CONSTRAINT `fk_Major_Department_School1` FOREIGN KEY (`SCHOOL_ID`) REFERENCES `mfha_school` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_news`
--
ALTER TABLE `mfha_news`
  ADD CONSTRAINT `fk_News_User1` FOREIGN KEY (`UPLOADED_BY`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_student_activity`
--
ALTER TABLE `mfha_student_activity`
  ADD CONSTRAINT `fk_Student_Activity_Activity1` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `mfha_activity` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Student_Activity_User1` FOREIGN KEY (`USER_ID`) REFERENCES `mfha_user` (`ID`);

--
-- Constraints for table `mfha_survey_answer`
--
ALTER TABLE `mfha_survey_answer`
  ADD CONSTRAINT `fk_Survey_Answer_Survey_Question1` FOREIGN KEY (`QUESTION_ID`) REFERENCES `mfha_survey_question` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Survey_Answer_User1` FOREIGN KEY (`USER_ID`) REFERENCES `mfha_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_survey_question`
--
ALTER TABLE `mfha_survey_question`
  ADD CONSTRAINT `fk_Survey_Answer_copy1_Activity1` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `mfha_activity` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mfha_user`
--
ALTER TABLE `mfha_user`
  ADD CONSTRAINT `fk_User_Major_Department1` FOREIGN KEY (`MAJOR_ID`) REFERENCES `mfha_major_department` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
