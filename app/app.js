/*
In NativeScript, the app.js file is the entry point to your application.
You can use this file to perform app-level initialization, but the primary
purpose of the file is to pass control to the app’s first module.
*/

require("./bundle-config");
var application = require("application");
var appSettings = require("application-settings");

// application.start({ moduleName: "views/talent-register" });
// application.start({ moduleName: "views/talent-vote" });
// application.start({ moduleName: "views/activity-staff" });


//Not yet login or already logout
if(!appSettings.hasKey("kUserInfo")) {
    application.start({ moduleName: "views/login" });
}
else {
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    if (userInfo["ROLE"] == 0) {
        //student
        application.start({ moduleName: "views/menu" });
    }
    else {  //FIXME: to be deleted, as another app
        //staff or admin
        application.start({ moduleName: "views/activity-staff" });
    }
}

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
