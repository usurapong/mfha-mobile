const appSettings = require("application-settings");
const frameModule = require("ui/frame");
const http = require("http");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");
const Color = require("color").Color;
const platform = require("platform");

//connection
const urlNews = mainURL.getURL("news");
const imgDir = mainURL.getURL("imageDir");
var lvNews, newsJSON;
const loader = new LoadingIndicator();

var newsArray = new observableArray([]);
var data = new observable.fromObject({
    //set binding data
    news: newsArray
});

exports.onLoaded = function (args) {
    //get current page
    const page = args.object;
    lvNews = page.getViewById("lvNews");

    //bind data with this page
    page.bindingContext = data;

    //if never load this page before or data is older than 1 hour
    if(!appSettings.hasKey("kNewsData") || mainModule.isExpire("kNewsTime", 1)) {
        //connect to server and get activity list        
        console.log("loading");
        connect();
    }
    else {
        //load saved data, not from server
        console.log("not load");
        newsJSON = JSON.parse(appSettings.getString("kNewsData"));
        newsArray.length = 0;
        newsArray.push(newsJSON);
    }
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

function connect() {
    loader.show();
    //request activity list for this academic year from server
    http.request({
        url: urlNews,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        //hide activity indicator
        loader.hide();
        //get header status
        const statusCode = response.statusCode;
        // console.log(statusCode);
        //convert response to JSON array
        newsJSON = response.content.toJSON();

        //no news
        if(statusCode != 200) {
            alert(newsJSON['status']);
            return;
        }
        
        //add image link
        newsArray.length = 0;
        for(var i=0; i<newsJSON.length; i++) {
            let title = newsJSON[i]['TITLE'];
            newsJSON[i]['TITLE'] = title.toUpperCase();
            newsJSON[i]['HEADER_PICTURE'] = imgDir + newsJSON[i]['HEADER_PICTURE'];
            newsArray.push(newsJSON[i]);
        }

        //save key to prevent reloading every time
        appSettings.setString("kNewsData", JSON.stringify(newsJSON));

        //save current time
        appSettings.setNumber("kNewsTime", Date.now());

        lvNews.notifyPullToRefreshFinished();
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvNews.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
    });
}

exports.reload = function() {
    console.log("pull to refresh");
    //reload data from DB
    connect();
}

exports.showNews = function(args) {
    // console.log(newsJSON[args.index]['TITLE']);
    //jump to news detail with selected news data
    frameModule.topmost().navigate({
        moduleName: "views/news-detail",
        //attached data
        context: {
            newsItem: newsJSON[args.index]
        },
        //navigation animation?
        animated: false
    });
}

exports.back = function () {
    frameModule.topmost().goBack();
}