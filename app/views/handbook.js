const frameModule = require("ui/frame");
const http = require("http");
const appSettings = require("application-settings");
const fs = require("file-system");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");
const Color = require("color").Color;
const platform = require("platform");

//connection
const urlHandbook = mainURL.getURL("handbook");
const urlDoc = mainURL.getURL("doc");

var lvHandbook, handbookJSON;
const loader = new LoadingIndicator();

var handbookArray = new observableArray([]);
var data = new observable.fromObject({
    //set binding data
    handbooks: handbookArray
});

exports.onLoaded = function (args) {
    //get current page
    const page = args.object;
    lvHandbook = page.getViewById("lvHandbook");

    //bind data with this page
    page.bindingContext = data;

    //if never load this page before or data is older than 5 hours
    if(!appSettings.hasKey("kHandbookData") || mainModule.isExpire("kHandbookTime", 5)) {
        //connect to server and get activity list
        console.log("loading");
        connect();
    }
    else {
        //load saved data, not from server
        console.log("not load");
        handbookJSON = JSON.parse(appSettings.getString("kHandbookData"));
        handbookArray.length = 0;
        handbookArray.push(handbookJSON);
    }
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

function connect() {
    loader.show();
    //request handbook list
    http.request({
        url: urlHandbook,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        loader.hide();
        //get header status
        const statusCode = response.statusCode;
        // console.log(statusCode);
        if(statusCode == 401) {
            alert("No handbook found");
            return;
        }

        //convert response to JSON array
        handbookJSON = response.content.toJSON();

        handbookArray.length = 0;
        handbookArray.push(handbookJSON);

        //save key to prevent reloading every time
        appSettings.setString("kHandbookData", response.content.toString());

        //save current time
        appSettings.setNumber("kHandbookTime", Date.now());

        lvHandbook.notifyPullToRefreshFinished();
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvHandbook.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
    });
}

exports.reload = function() {
    console.log("pull to refresh");
    //reload data from DB
    connect();
}

exports.back = function () {
    frameModule.topmost().goBack();
}

exports.showHandbook = function(args) {
    //get selected filename
    const filename = handbookJSON[args.index]["FILE_NAME"];

    //check if file exists
    const appFolder = mainURL.getURL("appFolder");
    const doc = fs.knownFolders.documents();
    const folder = doc.getFolder(appFolder);
    const path = fs.path.join(folder.path, filename);
    // //console.log(path);
    if (fs.File.exists(path)) {
        console.log("File exists, not load");
        viewHandbook(args.index, filename);
    }
    else {
        console.log("No pdf file, downloading...");
        loader.show();
        let fileURL = urlDoc + filename;

        //where to save file locally
        const appFolder = mainURL.getURL("appFolder");
        const doc = fs.knownFolders.documents();
        const folder = doc.getFolder(appFolder);
        const savedPath = fs.path.join(folder.path, filename);

        //download and save file to document's app folder
        http.getFile(fileURL, savedPath).then(function (r) {
            //// Argument (r) is File!
            loader.hide();
            console.log("Download complete");
            //jump to view page immediately?
            viewHandbook(args.index, filename);
        }, function (e) {
            //// Argument (e) is Error!
            loader.hide();
            alert("Download error, try again");
        });
    }
}

function viewHandbook(index, filename) {
    const navigationEntry = {
        moduleName: "views/handbook-view",
        context: {
            title: handbookJSON[index]["TITLE"],
            file: filename
        },
        animated: false
    };
    frameModule.topmost().navigate(navigationEntry);
}