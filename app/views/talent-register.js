const frameModule = require("ui/frame");
const appSettings = require("application-settings");
const dialogs = require("ui/dialogs");
const imagepicker = require("nativescript-imagepicker");
const imageSource = require("image-source");
const bghttp = require("nativescript-background-http");
const fs = require("file-system");
const http = require("http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");

var userInfo;
var memberArray = [];
var btnAddMember, imgPhoto, tfTeamTitle, tfPerfTitle, lblID, lblName, tfNickname, tfTel, tfFacebook, tfOther;
var tTitle, pfTitle, mNickname, mTel, mFacebook, pfType;
var userID, studentID;
var savedPath, savedPhoto = false;
var photoFilename;

const urlUploadImage = mainURL.getURL("uploadImage");
const urlRegisterTalent = mainURL.getURL("registerTalent");
const loader = new LoadingIndicator();

exports.onLoaded = function (args) {
    var page = args.object;
    btnAddMember = page.getViewById("btnAddMember");
    imgPhoto = page.getViewById("imgPhoto");
    tfTeamTitle = page.getViewById("tfTeamTitle");
    tfPerfTitle = page.getViewById("tfPerfTitle");
    lblName = page.getViewById("lblName");
    lblID = page.getViewById("lblID");
    tfNickname = page.getViewById("tfNickname");
    tfTel = page.getViewById("tfTel");
    tfFacebook = page.getViewById("tfFacebook");
    tfOther = page.getViewById("tfOther");

    userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    userID = userInfo["ID"];
    studentID = userInfo["USERNAME"];
    lblName.text = userInfo["FIRST_NAME"]+" "+userInfo["LAST_NAME"];
    lblID.text = studentID;

    if (appSettings.hasKey("kMember")) {
        memberArray = JSON.parse(appSettings.getString("kMember"));
        //show member numbers at button
        btnAddMember.text = "ADD MEMBER (" + memberArray.length + ")";
        //load first member info to fields
        tfNickname.text = memberArray[0]["nickname"];
        tfTel.text = memberArray[0]["tel"];
        tfFacebook.text = memberArray[0]["facebook"];
    }

    if (appSettings.hasKey("kTeam")) {
        const teamJSON = JSON.parse(appSettings.getString("kTeam"));
        tfTeamTitle.text = teamJSON["teamTitle"];
        tfPerfTitle.text = teamJSON["perfTitle"];
    }

    //check if user photo exists, load it
    const appFolder = mainURL.getURL("appFolder");
    photoFilename = mainURL.getURL("photoTalent");

    const doc = fs.knownFolders.documents();
    const folder = doc.getFolder(appFolder);
    savedPath = fs.path.join(folder.path, photoFilename);
    if (fs.File.exists(savedPath)) {
        // console.log("Load saved photo successful from " + path);
        savedPhoto = true;
        //show photo
        imgPhoto.src = savedPath;
    }
}

exports.addMember = function (args) {
    //validate all input fields
    if (!getInput()) {
        return;
    }

    //update first member info, usually the first array member
    memberArray[0] = { id: userID, 
        username: studentID, 
        nickname: mNickname, 
        tel: mTel,
        facebook: mFacebook 
    };
    appSettings.setString("kMember", JSON.stringify(memberArray));

    //save performance info to another key
    const perfInfo = { teamTitle: tTitle, perfTitle: pfTitle};
    appSettings.setString("kTeam", JSON.stringify(perfInfo));

    frameModule.topmost().navigate("views/talent-add");
}

exports.back = function () {
    frameModule.topmost().goBack();
}

exports.send = function () {
    const options = {
        title: "Confirm",
        message: "Are you sure to register? \nYour registration cannot be changed!",
        okButtonText: "Yes",
        cancelButtonText: "No"
    };

    //validate all input fields
    if (!getInput()) {
        return;
    }

    dialogs.confirm(options).then(function (result) {
        //result could be true/false/undefined             
        if (result == true) {
            send2server();
        }
    });
}

exports.pickImage = function (args) {
    //single image selection
    const context = imagepicker.create({
        mode: "single"
    });

    //request gallery access permission
    context.authorize()
        .then(function () {
            return context.present();
        })
        .then(function (selection) {
            //showing selected image use a bit of time, show loading indicator
            loader.show();
            const source = new imageSource.ImageSource();
            //for each selected image, here only one image allowed
            selection.forEach(function (selected) {
                //save image to document folder for loading later
                //this fromAsset() is for latest image picker plugin
                source.fromAsset(selected).then(function (imgSource) {
                    //show selected image                      
                    imgPhoto.imageSource = imgSource;
                    //save image to documents/appFolder/talent.png
                    savedPhoto = imgSource.saveToFile(savedPath, "png");
                    //if not succeeded, saved is FALSE
                    if (!savedPhoto) {
                        console.log("Save image failed");
                        alert("Problem saving image");
                        //revert image back
                        imgPhoto.src = "~/image/icon-addimage.png";
                    }
                    // else {
                    //     //show saved image                      
                    //     imgPhoto.imageSource = imgSource;
                    //     console.log("Save image done at " + path);
                    // }
                    loader.hide();
                });
            });
        })
        .catch(function (e) {
            console.log(e);
        });
}

function getInput() {
    //get input fields
    tTitle = tfTeamTitle.text.trim();
    pfTitle = tfPerfTitle.text.trim();
    mNickname = tfNickname.text.trim();
    mTel = tfTel.text.trim();
    mFacebook = tfFacebook.text.trim();
    
    //check input lengths
    if (tTitle.length == 0 || pfTitle.length == 0 || mNickname.length == 0 || mTel.length == 0 || mFacebook.length == 0) {
        alert("Please complete all input fields");
        return false;
    }

    //telephone digits
    if (mTel.length < 10) {
        alert("Please check your phone digits");
        return false;
    }

    //check photo
    if(!savedPhoto) {
        alert("Please choose your photo");
        return false;
    }

    return true;
}

//========================= Function: Insert registration info to server =======================
function send2server() {
    //performance info
    const perfInfo = { teamTitle: tTitle, perfTitle: pfTitle};
    const perfString = JSON.stringify(perfInfo);
    // console.log(perfString);
    appSettings.setString("kTeam", perfString);

    //update the first member info, if any change or no other members
    memberArray[0] = { id: userID, username: studentID, nickname: mNickname, tel: mTel, facebook: mFacebook };
    const memberString = JSON.stringify(memberArray);
    // console.log(memberString);
    appSettings.setString("kMember", memberString);
    // console.log(memberArray[0]["nickname"]);

    const contentToServer = "perfString=" + perfString + "&" + "memberString=" + memberString + "&" + "photoFilename=" + (studentID+"-"+photoFilename) + "&" + "year=" + userInfo["ACADEMIC_YEAR"];
    // console.log(contentToServer);

    //connect to server, insert data, upload photo and clear talent app settings (not necessary?)
    http.request({
        url: urlRegisterTalent,
        method: "POST",
        content: contentToServer,
    }).then(function (response) {
        loader.hide();

        //get response JSON content
        const result = response.content.toJSON();
        //get HTTP status code
        const statusCode = response.statusCode;

        //insertion failed?
        if (statusCode == 401) {
            alert(result['status']);
            return;
        }
        else {
            // console.log(result['status']);
            //upload image
            uploadImage();            
        }
    }, function (e) {
        loader.hide();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

//========================= Function: Upload image to server =======================
function uploadImage() {
    const session = bghttp.session("image-upload");
    const request = {
        url: urlUploadImage,
        method: "POST",
        headers: {
            "Content-Type": "application/octet-stream",
            "File-Name": (studentID+"-"+photoFilename)
        },
        description: "Uploading photo",
        androidAutoDeleteAfterUpload: false
    };

    loader.show();
    // const task = session.uploadFile("file://" + path, request);
    //upload file in background
    const task = session.uploadFile(savedPath, request);

    //logging progress
    task.on("progress", logEvent);
    task.on("error", logEvent);
    task.on("complete", logEvent);

    function logEvent(e) {
        console.log(e.eventName);
        if (e.eventName == "error") {
            loader.hide();
            alert("Cannot upload photo, try again!");
        }
        else if (e.eventName == "complete") {
            loader.hide();

            const alertOptions = {
                title: "Result",
                message: "Registration done",
                okButtonText: "OK",
                cancelable: false // [Android only]
            };
            
            alert(alertOptions).then(() => {
                //return to menu, need to put in dialog, otherwise it will not navigate while dialog is displaying [iOS]
                frameModule.topmost().navigate({
                    moduleName: "views/menu",
                    clearHistory: true
                });
            });           
        }
    }
}