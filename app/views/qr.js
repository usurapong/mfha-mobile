const frameModule = require("ui/frame");
const appSettings = require("application-settings");
const http = require("http");
const fs = require("file-system");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");

const loader = new LoadingIndicator();
const imgDir = mainURL.getURL("imageDir");

exports.onLoaded = function(args) {
    const page = args.object;
    var imgQR = page.getViewById("imgQR");
    var lblUsername = page.getViewById("lblUsername");

    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    lblUsername.text = userInfo['USERNAME'];
    // imgQR.src = imgDir + userInfo['QR_CODE'];

    //get selected filename
    const filename = userInfo['QR_CODE'];

    //check if file exists
    const doc = fs.knownFolders.documents();
    const folder = doc.getFolder(mainURL.getURL("appFolder"));
    const path = fs.path.join(folder.path, filename);
    if (fs.File.exists(path)) {
        console.log("QR File exists");
        imgQR.src = path;
    }
    else {
        console.log("No QR file, downloading");
        loader.show();
        const fileURL = imgDir + filename;
        http.getFile(fileURL, path).then(function (r) {
            //// Argument (r) is File!
            loader.hide();
            console.log("Download QR complete");
            imgQR.src = path;
        }, function (e) {
            //// Argument (e) is Error!
            loader.hide();
            alert("Download error, try again");
        });
    }
}

exports.back = function() {
    frameModule.topmost().goBack();
}