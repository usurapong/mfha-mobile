const frameModule = require("ui/frame");
const dialogs = require("ui/dialogs");
const http = require("http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");

//connection
const urlSurveyQuestion = mainURL.getURL("surveyQuestion");
var question;   //question response from server
const urlSurveyAnswer = mainURL.getURL("surveyAnswer");
const codeInsertFail = 401;
const urlUpdateSurveyStatus = mainURL.getURL("updateSurveyStatus");
const codeUpdateFail = 401;

const choice = ["Very Satisfied (5)", "Satisfied (4)", "Neutral (3)", "Unsatisfied (2)", "Very Unsatisfied (1)"];
var lpChoice, lblActivityName, lblActivityDate, lblQuestion, tvSuggest;
var data;   //received data
var loader = new LoadingIndicator();

exports.onLoaded = function(args) {    
    let page = args.object;
    lpChoice = page.getViewById("lpChoice");
    lblActivityName = page.getViewById("lblActivityName");
    lblActivityDate = page.getViewById("lblActivityDate");
    lblQuestion = page.getViewById("lblQuestion");
    tvSuggest = page.getViewById("tvSuggest");

    lblActivityName.text = data.activity_name;
    lblActivityDate.text = data.activity_date;

    lpChoice.items = choice;
    lpChoice.selectedIndex = 1;

    getQuestion();
}

exports.onNavigatingTo = function(args) {
    let page = args.object;
    //get data sent with navigation
    data = page.navigationContext;    
}

function getQuestion() {
    //load questions
    loader.show();
    http.request({
        url: urlSurveyQuestion + "/" + data.activity_id,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        loader.hide();
        question = response.content.toJSON();
        //for HLL, there will be only one ranking question and one open question
        //TODO: change for general cases
        lblQuestion.text = question[0]["TEXT"];
        tvSuggest.hint = question[1]["TEXT"];
    }, function (e) {
        loader.hide();
        //connection failed or other errors
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

exports.submit = function() {
    //confirm submit
    const options = {
        title: "Confirm",
        message: "Sending your survey result?",
        okButtonText: "Yes",
        cancelButtonText: "No"
    };
    dialogs.confirm(options).then(function(result) {
        // result can be true/false/undefined
        if(result) {
           submitSurvey(); 
        }
    });
    //do not place any code below here, otherwise it will be called before getting dialog answer
}

function submitSurvey() {
    loader.show();
    // alert(lpChoice.selectedIndex);
    //get survey result
    var answer_values1 = 5 - lpChoice.selectedIndex;
    var question_ID1 = question[0]["ID"];
    var user_id = data.user_id;
    // console.log(answer_values1+" "+question_ID1+" "+user_id);

    var answer_values2 = tvSuggest.text;
    var question_ID2 = question[1]["ID"];
    // console.log(answer_values2+" "+question_ID2+" "+user_id);

    //save survey result
    http.request({
        url: urlSurveyAnswer,
        method: "POST",
        timeout: 5000,
        content: "answer_values1=" + answer_values1 + "&question_ID1=" + question_ID1 + "&user_id=" + user_id + "&answer_values2=" + answer_values2 + "&question_ID2=" + question_ID2 + "&user_id=" + user_id
    }).then(function (response) {
        var statusCode = response.statusCode;
        if (statusCode == codeInsertFail) {
            alert("Sorry, try submit again!");
        }
        else {
            updateActivityStatus();
        }
    }, function (e) {
        loader.hide();
        //connection failed or other errors
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

function updateActivityStatus() {
    http.request({
        url: urlUpdateSurveyStatus + "/" + data.user_id,
        method: "PUT",
        timeout: 5000,
        content: "activity_id=" + data.activity_id
    }).then(function (response) {
        loader.hide();
        var statusCode = response.statusCode;
        if (statusCode == codeUpdateFail) {
            alert("Sorry, try submit again!");
        }
        else {
            alert("Survey is complete. Thank you!");

            //return to activity list
            frameModule.topmost().goBack();
        }
    }, function (e) {
        loader.hide();
        //connection failed or other errors
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

exports.back = function() {
    frameModule.topmost().goBack();
}