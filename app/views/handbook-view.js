const fs = require("file-system");
const frameModule = require("ui/frame");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");

const loader = new LoadingIndicator();

exports.onLoaded = function(args) {
    loader.show();
    const page = args.object;
    const data = page.navigationContext;

    let lblActionBar = page.getViewById("lblActionBar");
    lblActionBar.text = data.title;
    // page.actionBar.title = data.title;
    const pdfArea = page.getViewById("pdfArea");

    const appFolder = mainURL.getURL("appFolder");
    const doc = fs.knownFolders.documents();
    const folder = doc.getFolder(appFolder);
    const pdfPath = fs.path.join(folder.path, data.file);
    pdfArea.src = pdfPath;
}

//when pdf finishes loading
exports.onPDFload = function() {
    console.log("PDF shown");
    loader.hide();
}

exports.back = function() {
    frameModule.topmost().goBack();
}