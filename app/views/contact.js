const frameModule = require("ui/frame");
const http = require("http");
const appSettings = require("application-settings");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const utilsModule = require("utils/utils");
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");
const Color = require("color").Color;
const platform = require("platform");

//connection
const urlServer = mainURL.getURL("contact");
const imgDir = mainURL.getURL("imageDir");

var lvContact, contactJSON;
const loader = new LoadingIndicator();

var contactArray = new observableArray([]);

var data = new observable.fromObject({
    //set binding data
    contact: contactArray
});

exports.onLoaded = function (args) {
    //get current page
    const page = args.object;
    lvContact = page.getViewById("lvContact");

    //bind data with this page
    page.bindingContext = data;

    //if never load this page before
    if(!appSettings.hasKey("kContactData") || mainModule.isExpire("kContactTime", 5)) {
        //connect to server and get activity list
        console.log("loading");
        connect();
    }
    else {
        //load saved data, not from server
        console.log("not load");
        contactJSON = JSON.parse(appSettings.getString("kContactData"));
        contactArray.length = 0;
        contactArray.push(contactJSON);
    }
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

function connect() {
    loader.show();
    //request activity list for this academic year from server
    http.request({
        url: urlServer,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        //hide activity indicator
        loader.hide();
        //get header status
        const statusCode = response.statusCode;
        // console.log(statusCode);
        if(statusCode == 401) {
            alert("No contact found");
            return;
        }

        //convert response to JSON array
        contactJSON = response.content.toJSON();

        contactArray.length = 0;
        for(var i=0; i<contactJSON.length; i++) {
            contactJSON[i]['PICTURE'] = imgDir + contactJSON[i]['PICTURE'];
            contactArray.push(contactJSON[i]);
        }

        //save key to prevent reloading every time
        let contactString = JSON.stringify(contactJSON);

        // console.log(contactString);
        appSettings.setString("kContactData", contactString);

        //save current time
        appSettings.setNumber("kContactTime", Date.now());

        lvContact.notifyPullToRefreshFinished();
        lvContact.refresh();
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvContact.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
    });
}

exports.reload = function() {
    console.log("pull to refresh");
    //reload data from DB
    connect();
    // timer.setTimeout(function(){
    //     connect();
    // }, 1000);
}

exports.back = function () {
    frameModule.topmost().goBack();
}

exports.showContact = function(args) {
    // console.log(args.index);
    //jump to link
    utilsModule.openUrl(contactJSON[args.index]['URL']);
}