/*
    Objective: show user profile and logout
*/

//==================================== Module import ==============================
const frameModule = require("ui/frame");
const appSettings = require("application-settings");
const fs = require("file-system");
const mainURL = require("./mainURL");

//================================= Global Variables ==============================


//==================================== Events ==============================
//---------------- loaded event -----------------
exports.onLoaded = function(args) {
    const page = args.object;
    var lblID = page.getViewById("lblID");
    var lblName = page.getViewById("lblName");
    var lblMajor = page.getViewById("lblMajor");
    var lblSchool = page.getViewById("lblSchool");

    //load user profile
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    lblID.text = userInfo["USERNAME"];
    lblName.text = userInfo["FIRST_NAME"].toUpperCase() + " " + userInfo["LAST_NAME"].toUpperCase();
    lblMajor.text = userInfo["MAJOR_NAME"];
    lblSchool.text = userInfo["SCHOOL_NAME"];
}

//---------------- show QR image next page -----------------
exports.showQR = function() {
    frameModule.topmost().navigate("views/qr");
}

//---------------- return to menu -----------------
exports.back = function() {
    frameModule.topmost().goBack();
}

//---------------- logout -----------------
exports.logout = function() {
    //clear app settings
    appSettings.clear();

    //delete files in app folder
    const doc = fs.knownFolders.documents();
    const folder = doc.getFolder(mainURL.getURL("appFolder"));
    //if folder exists
    if(folder) {
        // Remove a folder and recursively its content.
        folder.remove()
            .then((res) => {
                // Success removing the folder.
                console.log("Folder successfully deleted!");
            }).catch((err) => {
                console.log(err.stack);
            });
    } else {
        console.log("No folder deleted!");
    }

    //return to login page and clear all history
    frameModule.topmost().navigate({
        moduleName: "views/login",
        clearHistory: true
    });
}