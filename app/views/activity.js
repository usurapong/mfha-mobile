/*
    Objective: show user profile and logout
    Ref for search bar: https://stackoverflow.com/questions/36005172/nativescript-how-can-i-filter-an-observable-array-with-searchbar
*/

//==================================== Module import ==============================
const frameModule = require("ui/frame");
const http = require("http");
const appSettings = require("application-settings");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const ModalPicker = require("nativescript-modal-datetimepicker").ModalDatetimepicker;
const Color = require("color").Color;
const platform = require("platform");

// const dialogs = require("ui/dialogs");
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");

//================================= Variables declaration ==============================
//connection
const urlActivity = mainURL.getURL("activity");
const loader = new LoadingIndicator();
const picker = new ModalPicker();
const arrColor = ["#323232", "#e9a929", "#34d955", "#eb4949"];
var page, lvActivity, lblFrom, lblTo, searchBar;
var keyword="";
var startDate=null, endDate=null;
var userID;
var lvActivity, activityJSON;
var activityArray = new observableArray([]);
var data = new observable.fromObject({
    //set binding data
    activities: activityArray
});

//==================================== Events ==============================
//---------------- loaded event -----------------
exports.onLoaded = function (args) {
    //get current page
    page = args.object;
    searchBar = page.getViewById("searchBar");
    lvActivity = page.getViewById("lvActivity");
    lblFrom = page.getViewById("lblFrom");
    lblTo = page.getViewById("lblTo");

    //bind data with this page
    page.bindingContext = data;

    //hide keyboard of searchBar
    hideSearchKeyboard();

    //bind searchBar to function
    searchBar.on("textChange", onSearch);

    //if never load this page before
    if(!appSettings.hasKey("kAcvitityData") || mainModule.isExpire("kActivityTime", 1)) {
        //connect to server and get activity list
        console.log("loading");
        connect();
    }
    else {
        //load saved data, not from server
        console.log("not load");
        activityJSON = JSON.parse(appSettings.getString("kAcvitityData"));
        activityArray.length = 0;
        activityArray.push(activityJSON);
    }
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

//---------------- connect to server -----------------
function connect() {
    loader.show();
    //get academic year related to user's ID
    //load student ID and userID from app setting
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    const studentID = userInfo["USERNAME"];
    const acYear = userInfo["ACADEMIC_YEAR"];
    // var acYear = parseInt("25"+studentID.substring(0,2)) - 543;
    userID = userInfo["ID"];
    console.log(acYear+" "+userID);

    //request activity list for this academic year from server
    http.request({
        url: urlActivity + "?userID=" + userID + "&year=" + acYear,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        //hide activity indicator
        loader.hide();

        const statusCode = response.statusCode;
        // console.log(statusCode);
        if(statusCode != 200) {
            alert(result["status"]);
            return;
        }

        //convert response to JSON array
        activityJSON = response.content.toJSON();

        activityArray.length = 0;
        //change datetime format and activity color label
        for (i = 0; i < activityJSON.length; i++) {
            // reformat time
            const date = new Date(activityJSON[i]["SCANNING_STARTDATETIME"].replace(/-/g, "/"));
            activityJSON[i]["DATE"] = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

            //change activity color
            if (activityJSON[i]["STATUS_ACTIVITY"] == null) {
                //this activity is not yet opened or not joined
                activityJSON[i]["STATUS_ACTIVITY"] = arrColor[0];
            }
            else {
                activityJSON[i]["STATUS_ACTIVITY"] = arrColor[activityJSON[i]["STATUS_ACTIVITY"]];
            }
            activityArray.push(activityJSON[i]);
        }

        //save key to prevent reloading every time
        appSettings.setString("kAcvitityData", JSON.stringify(activityJSON));

        //save current time
        appSettings.setNumber("kActivityTime", Date.now());

        lvActivity.notifyPullToRefreshFinished();
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvActivity.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

//---------------- Listview: pull to refresh -----------------
exports.reload = function() {
    console.log("pull to refresh");
    clearSearch();
    //reload data from DB
    connect();
}

//---------------- return to menu -----------------
exports.back = function () {
    frameModule.topmost().goBack();
}

//---------------- touch listview item and jump to survey -----------------
exports.showActivity = function (args) {
    //if student activity status is not attended (1), skip
    //this is for How to Live&Learn only
    if(activityJSON[args.index]["STATUS_ACTIVITY"] != arrColor[1]) {
        console.log("Only status 1 (orange): attended and waiting for fill in survey!");
        return;
    }

    //get current datetime
    var dateNow = new Date();

    //check whether scan or survey
    //scan period
    var dateScanStart = new Date(activityJSON[args.index]["SCANNING_STARTDATETIME"].replace(/-/g, "/"));
    var dateScanEnd = new Date(activityJSON[args.index]["SCANNING_ENDDATETIME"].replace(/-/g, "/"));
    //survey period
    var dateSurveyStart = new Date(activityJSON[args.index]["SURVEY_STARTDATETIME"].replace(/-/g, "/"));
    var dateSurveryEnd = new Date(activityJSON[args.index]["SURVEY_ENDDATETIME"].replace(/-/g, "/"));

    // if (dateNow.getTime() >= dateScanStart.getTime() && dateNow.getTime() <= dateScanEnd.getTime()) {
    //     alert("Scan by student");
    // } else

    if (dateNow.getTime() >= dateSurveyStart.getTime() && dateNow.getTime() <= dateSurveryEnd.getTime()) {
        //go to survey
        //attach data: ACTIVITY_ID, DATE, ACTIVITY_NAME
        const navigationEntry = {
            moduleName: "views/survey",
            context: {
                activity_id: activityJSON[args.index]["ID"],
                activity_date: dateScanStart.getDate() + "/" + (dateScanStart.getMonth() + 1) + "/" + dateScanStart.getFullYear(),
                activity_name: activityJSON[args.index]["TITLE"],
                user_id: userID
            },
            animated: false
        };
        frameModule.topmost().navigate(navigationEntry);
    }
    else {
        alert("Sorry, too late to complete the survey!");
    }
}

//---------------- modal page to show activity color label -----------------
exports.showLabel = function (args) {
    //show page as a modal
    page.showModal("views/activity-modal", "Color Label",
        function closeCallback() {
            //do nothing
    });
}

//---------------- Use modal-datetimepicke plugin for month picker -----------------
exports.selectBeginMonth = function () {
    var mxDate = new Date();
    startDate = new Date();
    if(!endDate) {
        mxDate.setDate(mxDate.getDate()+365);
    }
    else {
        startDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
        mxDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
    }

    picker.pickDate({
        title: "Select starting date",
        theme: "light",
        startingDate: startDate,
        maxDate: mxDate
    }).then((result) => {
        startDate = new Date(result.year, result.month-1, result.day);
        lblFrom.text = "From " + result.day + "/" + result.month + "/" + result.year;
        //filter by date
        filterDate();
    }).catch((error) => {
        console.log("Error: " + error);
    });
}

exports.selectEndMonth = function () {
    var mnDate = new Date();
    endDate = new Date();
    if(!startDate) {
        mnDate.setDate(mnDate.getDate() - 365);
    }
    else {
        endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
        mnDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
    }
    picker.pickDate({
        title: "Select ending date",
        theme: "light",
        startingDate: endDate,
        minDate: mnDate
    }).then((result) => {
        endDate = new Date(result.year, result.month-1, result.day);
        lblTo.text = "To " + result.day + "/" + result.month + "/" + result.year;
        //filter by date
        filterDate();
    }).catch((error) => {
        console.log("Error: " + error);
    });
}

//whenever user change value in search bar
function onSearch(args) {
    keyword = args.object.text.trim();
    filterDate();
    // activityArray.length = 0;
    // //filter
    // for (i = 0; i < activityJSON.length; i++) {
    //     let title = activityJSON[i]["TITLE"];
    //     //if keyword is found
    //     if(title.toLowerCase().indexOf(keyword) != -1) {
    //         activityArray.push(activityJSON[i]);
    //     }
    // }
}

function clearSearch() {
    searchBar.text = "";
    keyword = "";
    lblFrom.text = "From DD/MM/YY";
    lblTo.text = "To DD/MM/YY";
    startDate = null;
    endDate = null;
    hideSearchKeyboard();
}

function hideSearchKeyboard() {
    if (searchBar.ios) {
        searchBar.ios.endEditing(true);
    } else if (searchBar.android) {
        searchBar.android.clearFocus();
    }
}

exports.onClear = function(args) {
    clearSearch();
    activityArray.length = 0;
    activityArray.push(activityJSON);
}

//when user select begin or end month
function filterDate() {
    // var add = true;
    activityArray.length = 0;
    // console.log("keyword = "+keyword);

    //filter
    for (i = 0; i < activityJSON.length; i++) {
        //filter by keyword
        let title = activityJSON[i]["TITLE"];
        // console.log("Title = "+title);
        //if keyword is not blank but it is not found
        if(keyword!="" && title.toLowerCase().indexOf(keyword) == -1) {
            // add = false;
            continue;
        }
        // console.log("add = "+add);

        const activityDate = new Date(activityJSON[i]["SCANNING_STARTDATETIME"].replace(/-/g, "/"));        
        if(!startDate && !endDate) {
            //if keyword found and no start or end month setting
            activityArray.push(activityJSON[i]);
        }
        else if(!startDate) {
            // console.log("case 2");
            //if keyword found and only end date is set
            //filter only activity <= end date
            if(activityDate.getTime() <= endDate.getTime()) {
                activityArray.push(activityJSON[i]);
            }
        }
        else if(!endDate) {
            // console.log("case 3");
            //if keyword found and only start date is set
            //filter only activity >= start date
            if(activityDate.getTime() >= startDate.getTime()) {
                activityArray.push(activityJSON[i]);
            }
        }
        else {
            // console.log("case 4");
            //if keyword found, and both start and end date are set
            if(activityDate.getTime() >= startDate.getTime() && activityDate.getTime() <= endDate.getTime()) {
                activityArray.push(activityJSON[i]);
            }
        }
    }
}