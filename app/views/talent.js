const frameModule = require("ui/frame");
const http = require("http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const appSettings = require("application-settings");
const mainURL = require("./mainURL");

const urlRegister = mainURL.getURL("register");
const urlVote = mainURL.getURL("vote");
const loader = new LoadingIndicator();

exports.register = function () {
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    const studentID = userInfo["USERNAME"];

    loader.show();
    //check if already registered and setting is allowed
    http.request({
        url: urlRegister + "/" + studentID,
        method: "GET"
    }).then(function (response) {
        loader.hide();
        //get response JSON content
        const result = response.content.toJSON();
        //get HTTP status code
        const statusCode = response.statusCode;
        if (statusCode == 401) {
            alert(result["error_text"]);
            return;
        }

        //jump
        frameModule.topmost().navigate("views/talent-register");
    }, function (e) {
        loader.hide();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

exports.vote = function () {
    // console.log("vote");
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    const studentID = userInfo["ID"];
    const year = userInfo["ACADEMIC_YEAR"];

    loader.show();
    //check if 1) setting is allowed, 2) not yet vote and 3) 80% survey
    // console.log(urlVote + "?userID=" +studentID+ "&year=" +year);
    http.request({
        url: urlVote + "?userID=" +studentID+ "&year=" +year,
        method: "GET"
    }).then(function (response) {
        loader.hide();
        //get response JSON content
        const result = response.content.toJSON();
        //get HTTP status code
        const statusCode = response.statusCode;
        // console.log(statusCode);
        if (statusCode == 401) {
            alert(result["status"]);
            return;
        }

        //jump
        frameModule.topmost().navigate("views/talent-vote");
    }, function (e) {
        loader.hide();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

exports.back = function () {
    frameModule.topmost().goBack();
}