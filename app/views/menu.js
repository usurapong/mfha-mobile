/*
    Objective: main menu with news carousel
*/

//==================================== Module import ==============================
const appSettings = require("application-settings");
const frameModule = require("ui/frame");
const http = require("http");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");
const Color = require("color").Color;
const platform = require("platform");

//================================= Variables declaration ==============================
const loader = new LoadingIndicator();
//connection
const urlNews = mainURL.getURL("news");
const imgDir = mainURL.getURL("imageDir");
//for listview
var newsArray = new observableArray([]);
var data = new observable.fromObject({
    news: newsArray
});

var lvNews = null, newsJSON = null;
const maxNewsItems = 5;

//==================================== Events ==============================
//---------------- loaded event -----------------
exports.onLoaded = function(args) {
    // console.log("screen height = " + platform.screen.mainScreen.heightPixels);
    var page = args.object;

    //show username
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    var lblActionBar = page.getViewById("lblActionBar");
    lblActionBar.text = "Hi, " + (userInfo["FIRST_NAME"]).toUpperCase();
    //page.actionBar.title = "Hi "+userInfo["FIRST_NAME"];

    lvNews = page.getViewById("lvNews");

    //bind data with this page
    page.bindingContext = data;

    //if never load this page before or data is older than 1 hour
    if(!appSettings.hasKey("kNews5Data") || mainModule.isExpire("kNews5Time", 1)) {
        //connect to server and get activity list
        console.log("loading");
        connect();
    }
    else {
        //load saved data, not from server
        console.log("not load");
        newsJSON = JSON.parse(appSettings.getString("kNews5Data"));

        //load JSON to listview's array
        newsArray.length = 0;
        newsArray.push(newsJSON);
    }
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

//---------------- connect to server -----------------
function connect() {
    loader.show();
    //request latest 5 news
    http.request({
        url: urlNews + "?number=" + maxNewsItems,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        //hide activity indicator
        loader.hide();
        //get header status
        const statusCode = response.statusCode;
        //convert response to JSON array
        newsJSON = response.content.toJSON();

        //no news
        if(statusCode != 200) {
            alert(newsJSON['status']);
            return;
        }

        //add image link
        newsArray.length = 0;
        for(var i=0; i<newsJSON.length; i++) {
            let title = newsJSON[i]['TITLE'];
            newsJSON[i]['TITLE'] = title.toUpperCase();
            newsJSON[i]['HEADER_PICTURE'] = imgDir + newsJSON[i]['HEADER_PICTURE'];
            newsArray.push(newsJSON[i]);
        }

        //save key to prevent reloading every time
        appSettings.setString("kNews5Data", JSON.stringify(newsJSON));

        //save current time
        appSettings.setNumber("kNews5Time", Date.now());

        lvNews.notifyPullToRefreshFinished();
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvNews.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
    });
}

//---------------- Listview: pull to refresh -----------------
exports.reload = function() {
    console.log("pull to refresh");
    //reload data from DB
    connect();
}

//----------------  touch carousel image -----------------
exports.showNews = function(args) {
    //jump to news details
    frameModule.topmost().navigate({
        moduleName: "views/news-detail",
        //attached data
        context: {
            newsItem: newsJSON[args.index]
        },
        //navigation animation?
        animated: false
    });
}

//----------------  menu jumping -----------------
exports.showActivity = function(args) {
    frameModule.topmost().navigate("views/activity");
}

exports.showProfile = function(args) {
    frameModule.topmost().navigate("views/profile");
}

exports.listNews = function(args) {
    frameModule.topmost().navigate("views/news");
}

exports.showHandbook = function(args) {
    frameModule.topmost().navigate("views/handbook");
}

exports.showContact = function(args) {
    frameModule.topmost().navigate("views/contact");
}

exports.showTalent = function(args) {
    frameModule.topmost().navigate("views/talent");
}