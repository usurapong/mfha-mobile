/*
    Objective: login validation and getting user info
*/

//==================================== Module import ==============================
const http = require("http");
const appSettings = require("application-settings");
const frameModule = require("ui/frame");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const dialogs = require("ui/dialogs");
const mainURL = require("./mainURL");

//================================= Variables declaration ==============================
var tfUsername, tfPassword;
const loader = new LoadingIndicator();
//server reference
const urlLogin = mainURL.getURL("login");

//==================================== Events ==============================
//---------------- loaded event -----------------
exports.onLoaded = function (args) {
    const page = args.object;
    //bind UI to variables
    tfUsername = page.getViewById("tfUsername");
    tfPassword = page.getViewById("tfPassword");
};

//---------------- login tap event -----------------
exports.login = function () {
    //user inputs
    username = tfUsername.text.trim();
    password = tfPassword.text.trim();

    //blank input?
    if (username.length == 0 || password.length == 0) {
        alert("Missing username or password");
        return;
    }

    //validate with server
    loader.show();
    http.request({
        //POST username and password
        url: urlLogin,
        method: "POST",
        timeout: 5000,
        content: "username=" + username + "&" + "password=" + password,
    }).then(function (response) {
        //response retrieved
        loader.hide();
        //get HTTP status code
        const statusCode = response.statusCode;
        //get response JSON content
        const result = response.content.toJSON();

        //wrong login?
        if (statusCode != 200) {
            // console.log(statusCode);
            //dialog
            const options = {
                title: "Incorrect",
                message: result["status"],
                okButtonText: "OK"
            };
            dialogs.alert(options).then(function() {
                // console.log("Choose OK");
            });
            return;
        }

        //correct login
        //save user info
        appSettings.setString("kUserInfo", response.content.toString());
        //check user role: student(0) or staff(1)
        if (result["ROLE"] == 0) {
            //student
            frameModule.topmost().navigate({
                moduleName: "views/menu",
                clearHistory: true
            });
        }
        else {
            //staff or admin
            // console.log("staff");
            frameModule.topmost().navigate({
                moduleName: "views/activity-staff",
                clearHistory: true
            });
        }
    }, function (e) {
        //connection failed
        loader.hide();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
};