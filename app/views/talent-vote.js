const frameModule = require("ui/frame");
const http = require("http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const appSettings = require("application-settings");
const observable = require("data/observable");
const mainURL = require("./mainURL");
const Color = require("color").Color;
const platform = require("platform");

const urlTalentTeam = mainURL.getURL("talentTeam");
const imgDir = mainURL.getURL("imageDir");
const loader = new LoadingIndicator();

var data = new observable.fromObject({
    //set binding data
    candidate: []
});

var result; //to keep response result from server

//when page is loaded
exports.onLoad = function(args) {
    //get current page
    var page = args.object;

    //connect to server
    connect();

    //bind data with this page
    page.bindingContext = data;
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

exports.showCandidate = function(args) {
    // console.log(args.index);
    frameModule.topmost().navigate({
        moduleName: "views/talent-vote-detail",
        //attached data
        context: {
            teamInfo: result[args.index]
        },
        //navigation animation?
        animated: false
    });
}

exports.back = function () {
    frameModule.topmost().goBack();
}

function connect() {
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    const year = userInfo["ACADEMIC_YEAR"];
    loader.show();
    //get all accepted teams
    http.request({
        url: urlTalentTeam + "/" + year,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        loader.hide();
        //get response JSON content
        result = response.content.toJSON();
        //get HTTP status code
        const statusCode = response.statusCode;
        if (statusCode == 401) {
            alert(result["error_text"]);
            return;
        }

        //add image folder to image file
        for(var i=0; i<result.length; i++) {
            result[i]["AEDO_PICTURE"] = imgDir + result[i]["AEDO_PICTURE"];
        }

        //add to listview
        data.set("candidate", result);
    }, function (e) {
        loader.hide();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}