const utilsModule = require("utils/utils");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const frameModule = require("ui/frame");
const mainURL = require("./mainURL");

const loader = new LoadingIndicator();
const imgDir = mainURL.getURL("imageDir");
var newsJSON = null;
var lblActionBar = null, imgNews = null;

exports.onNavigatedTo = function(args) {
    loader.show();
    const page = args.object;
    newsJSON = page.navigationContext.newsItem;

    // page.actionBar.title = newsJSON['TITLE'];
    lblActionBar.text = newsJSON['TITLE'];
    imgNews.src = imgDir + newsJSON['DETAIL_PICTURE'];
    loader.hide();
}

exports.onLoaded = function (args) {
    imgNews = args.object.getViewById("imgNews");
    lblActionBar = args.object.getViewById("lblActionBar");
}

exports.showLink = function(args) {
    //jump to link
    utilsModule.openUrl(newsJSON['URL_LINK']);
}

exports.back = function () {
    frameModule.topmost().goBack();
}