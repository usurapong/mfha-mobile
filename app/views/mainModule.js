const appSettings = require("application-settings");

//---------------- Check whether the save time expired -----------------
exports.isExpire = function(key, timeExpire) {
    //convert expire time from hour to millisecond
    timeExpire = timeExpire*60*60*1000;
    var expire = false;
    if(!appSettings.hasKey(key)) {
        expire = true;
    }
    else {
        const timeDiff = Date.now() - appSettings.getNumber(key);        
        if(timeDiff > timeExpire) {
            expire = true;
        }               
    }
    console.log("Expire = " + expire);
    return expire;
}