const frameModule = require("ui/frame");
const http = require("http");
const appSettings = require("application-settings");
const observable = require("data/observable");
const dialogs = require("ui/dialogs");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");

const loader = new LoadingIndicator();
var memberArray = [];

var data = new observable.fromObject({
    members: memberArray
});

const urlUserTalent = mainURL.getURL("userTalent");
var lvMember, tfID, tfNickname, tfTel, tfFacebook;
var mNickname, mTel, mFacebook;

exports.onLoaded = function(args) {
    var page = args.object;
    lvMember = page.getViewById("lvMember");
    tfID = page.getViewById("tfID");
    tfNickname = page.getViewById("tfNickname");
    tfTel = page.getViewById("tfTel");
    tfFacebook = page.getViewById("tfFacebook");

    page.bindingContext = data;

    //is previous data members existed?
    if(appSettings.hasKey("kMember")) {
        memberArray = JSON.parse(appSettings.getString("kMember"));
        data.set("members", memberArray);
        lvMember.refresh();
    }
}

exports.add = function() {
    const studentID = tfID.text.trim(); 
    if(studentID.length == 0) {
        alert("Incomplete input!");
        return;
    }
    
    //check if user already existed in listview
    let found = false;
    for(let i=0; i<memberArray.length;i++) {
        let row = memberArray[i];
        if(row["username"] == studentID) {
            found = true;
            break;
        }
    }

    //repeated
    if(found) {
        alert("Already existed!");
        return;                
    }

    //all fields required
    mNickname = tfNickname.text.trim();
    mTel = tfTel.text.trim();
    mFacebook = tfFacebook.text.trim();
    if(studentID.length==0 || mNickname.length==0 || mTel.length==0 || mFacebook.length==0) {
        alert("Error, Incomplete input");
        return;
    }

    //telephone digits
    if (mTel.length < 10) {
        alert("Please check your phone digits");
        return;
    }
    
    //connect to server, is this new ID valid and have not registered already?
    //request major and school
    loader.show();
    http.request({
        url: urlUserTalent + "/" + studentID,
        method: "GET",
        timeout: 5000
    }).then(function (response) {
        loader.hide();
        const statusCode = response.statusCode;
        const userJSON = response.content.toJSON();
        if(statusCode == 401) {
            alert(userJSON["error_text"]);
            return;
        }

        //get user ID for later reference
        const userID = userJSON["ID"];

        //update listview
        memberArray.push({id:userID, username:studentID, nickname:mNickname, tel:mTel, facebook:mFacebook});
        lvMember.refresh();      
    }, function (e) {
        loader.hide();
        //connection failed or other errors
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });

    // tfID.text = "";
}

exports.delete = function(args) {
    //delete member?
    const options = {
        title: "Warning",
        message: "Are you sure to remove this person?",
        okButtonText: "Yes",
        cancelButtonText: "No"
    };

    if(args.index > 0) {
        dialogs.confirm(options).then(function(result) {
            //result could be true/false/undefined             
            if(result==true) {
                //remove only that index
                memberArray.splice(args.index, 1);
                lvMember.refresh();
            }
        });
    }
}

exports.confirm = function() {
    const options = {
        title: "Confirm",
        message: "Save these members?",
        okButtonText: "Yes",
        cancelButtonText: "No"
    };

    dialogs.confirm(options).then(function(result) {
        //result could be true/false/undefined             
        if(result==true) {
            //save members to app setting
            //convert array to JSON string
            const strMember = JSON.stringify(memberArray);
            appSettings.setString("kMember", strMember);

            //jump to previous page with data and clear history
            frameModule.topmost().goBack();
        }
    });    
}

exports.back = function () {
    frameModule.topmost().goBack();
}