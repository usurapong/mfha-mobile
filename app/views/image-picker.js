const imagepicker = require("nativescript-imagepicker");
const fs = require("file-system");
const bghttp = require("nativescript-background-http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const imageSource = require("image-source");

const loader = new LoadingIndicator();
var imgPhoto, lblImage, path, saved=false;
const filename = "talent.png";

exports.load = function (args) {
    imgPhoto = args.object.getViewById("imgPhoto");
    lblImage = args.object.getViewById("lblImage");

    //check if image exists, load it
    const folder = fs.knownFolders.documents();
    path = fs.path.join(folder.path, filename);
    if (fs.File.exists(path)) {
        console.log("Load successful from " + path);
        saved = true;
        imgPhoto.src = path;
    }
}

exports.choose = function (args) {
    const context = imagepicker.create({
        mode: "single"
    });
    context.authorize()
        .then(function () {
            return context.present();
        })
        .then(function (selection) {
            selection.forEach(function (selected) {
                // alert("uri: " + selected.uri+ "\n" + selected.android);
                // imgPhoto.src = selected.android;
                // lblImage.text = selected.android;

                //save image to file for loading later
                selected.getImage().then(function (imgSource) {
                    const folder = fs.knownFolders.documents();
                    path = fs.path.join(folder.path, filename);
                    saved = imgSource.saveToFile(path, "png");
                    if (saved) {
                        console.log("Save successful at " + path);                        
                    }
                });
            });
        })
        .catch(function (e) {
            console.log(e);
        });
}

exports.upload64 = function() {
    //convert image to base64 string
    const img = imageSource.fromFile(path);
    const base64String = img.toBase64String("png");
    console.log(base64String.length);
    //this cause a larger 33% of image size string! simple to save to DB but is it OK? A large image could > 100K length
}

exports.upload = function() {
    if(!saved) {
        alert("Please select image first!");
        return;
    }
    const session = bghttp.session("image-upload");
    const request = {
        url: "http://10.0.3.2/mfha/uploadImageTalent",
        method: "POST",
        headers: {
            "Content-Type": "application/octet-stream",
            "File-Name": filename
        },
        description: "test uploading",
        androidAutoDeleteAfterUpload: false
    };

    // const task = session.uploadFile("file://" + path, request);
    loader.show();
    const task = session.uploadFile(path, request);
    task.on("progress", logEvent);
    task.on("error", logEvent);
    task.on("complete", logComplete);
    function logEvent(e) {
        console.log(e.eventName);
    }
    function logComplete(e) {
        console.log(e.eventName);
        loader.hide();
    }
}