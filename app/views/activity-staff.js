/*
    Staff functions
    Objective: show today activity, or all activities, upload and logout
    Manual:
        -Use today menu (default)
        -Touch for bulk scan
        -Scan QR code
        -Default exit: reaching max scan limit
        -Manual exit: Android press back key
        -After exiting, data checked and auto saved
        -Long touch to upload to server, old data cleared
    FAQ:
        -How to check that each scan is OK?
            -observe vibration, beep and toast message
        -Can I scan the same student more than one time?
            -No, the app auto checks
        -Why can't I scann the student?
            -Try to adjust scan distance or even change the mobile device
        -How do I know I already scan this student?
            -To make sure, exit the current scan then rescan and exit. The app shows repeat error message.
        -Is it OK if many staffs scan the same student?
            -No problem, the upload process takes care of that.
*/

//==================================== Module import ==============================
const frameModule = require("ui/frame");
const http = require("http");
const appSettings = require("application-settings");
const observable = require("data/observable");
const observableArray = require("data/observable-array").ObservableArray;
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const mainURL = require("./mainURL");
const mainModule = require("./mainModule");
const BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
const dialogs = require("ui/dialogs");
const Toast = require("nativescript-toast");
const Vibrate = require("nativescript-vibrate").Vibrate;
const Color = require("color").Color;
const platform = require("platform");
//to use vibrate plugin in Android, must add permission manually
//<uses-permission android:name="android.permission.VIBRATE" />
//in D:\Codes\Nativescript\HLL\app\App_Resources\Android\AndroidManifest.xml

//================================= Variables declaration ==============================
//connection
const urlActivityNow = mainURL.getURL("activity_now");
const urlActivityYear = mainURL.getURL("activity_year");
const urlUploadQR = mainURL.getURL("uploadQR");
//plugins
const loader = new LoadingIndicator();
const barcodescanner = new BarcodeScanner();
const vibrator = new Vibrate();
//others
var page, lvActivity;
var userInfo;
var lblHeader, lvActivity, activityJSON;
var activityArray = new observableArray([]);
var data = new observable.fromObject({
    //set binding data
    activities: activityArray
});
const keyScanPrefix = "kscan000";
var canScan = false;    //flag to allow to scan only today activity
var pullToday = true;   //flag for pull to refresh either today or all activities
var returnFromScan = false; //flag to check whether the onLoad function is auto called after finish scanning

//==================================== Events ==============================
//---------------- loaded event -----------------
exports.onLoaded = function (args) {
    //get current page
    page = args.object;

    //show user name on title bar
    userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    let lblActionBar = page.getViewById("lblActionBar");
    lblActionBar.text = (userInfo["FIRST_NAME"]).toUpperCase();

    lblHeader = page.getViewById("lblHeader");
    lblHeader.text = "Today Activities";
    //listview
    lvActivity = page.getViewById("lvActivity");

    //bind data with this page
    page.bindingContext = data;

    //load activities from DB and update listview except when return from scan
    if(!returnFromScan) {
        connectDB("now");
    }
    returnFromScan = false;
}

//iOS only to make trasparent Listview
exports.onItemLoading = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.ios.backgroundView.backgroundColor = newcolor.ios;
    }
}

//iOS only to make trasparent pull-to-refresh area of Listview
exports.onItemLoaded = function(args) {
    if(platform.isIOS) {
        let newcolor = new Color(0,0,0,0);
        args.object.ios.pullToRefreshView.backgroundColor = newcolor.ios;
    }
}

//---------------- connect to server -----------------
function connectDB(period) {
    //default is today activity
    var urlActivity = urlActivityNow;
    canScan = true;
    lblHeader.text = "Today Activities";

    //in case all activities
    if(period == "all") {
        urlActivity = urlActivityYear;
        canScan = false;
        lblHeader.text = "All Activities";
    }

    //get current year and assume to be academic year
    //cannot use academic year of staff!
    // const acYear = userInfo["ACADEMIC_YEAR"];
    const acYear = (new Date()).getFullYear();

    loader.show();
    //request activity list for this academic year from server
    http.request({
        url: urlActivity + "/" + acYear,
        method: "GET",
        timeout: 5000
    }).then(function (response) {               
        const statusCode = response.statusCode;
        // console.log(statusCode);
        //convert response to JSON array
        activityJSON = response.content.toJSON();
        // console.log(response.content.toString());
        //hide activity indicator
        loader.hide();
        lvActivity.notifyPullToRefreshFinished(); 

        // console.log(statusCode);
        if(statusCode == 401) {
            //No activity
            alert(activityJSON["status"]);
            //clear listview
            activityArray.length = 0;
            return;
        }

        //Some activities today
        //load previous scan and update badge
        refreshBadge();       
    }, function (e) {
        //connection failed or other errors
        loader.hide();
        lvActivity.notifyPullToRefreshFinished();
        alert("Error, please check your Internet connection");
        console.log("Error" + e);
    });
}

//---------------- Listview: pull to refresh -----------------
exports.reload = function() {    
    //reload data from DB
    if(pullToday) {
        console.log("pull to refresh, today");
        connectDB("now");
    }
    else {
        console.log("pull to refresh, all");
        connectDB("all");
    }
}

exports.showToday = function() {
    pullToday = true;
    connectDB("now");
}

exports.showAll = function() {
    pullToday = false;
    connectDB("all");
}

//---------------- return to menu -----------------
exports.logout = function () {
    //confirm dialog
    const options = {
        title: "Logout",
        message: "Sure? All not uploaded data will be deleted!",
        okButtonText: "Yes",
        cancelButtonText: "Cancel"
    };

    dialogs.confirm(options).then(function(result) {
        if(result == true) {
            //clear app settings
            appSettings.clear();
            //return to login page and clear all history
            frameModule.topmost().navigate({
                moduleName: "views/login",
                clearHistory: true
            });
        }
    });
}

//--------------- bulk scan QR codes -------------
exports.scanQR = function(args) {
    //if NOT today activity, can't scan
    if(!canScan) {
        console.log("Cannot scan, not today activity");
        return;
    }

    const MAX_SCAN = 3;
    var scanCount = 0;
    var arrScan = [];

    barcodescanner.scan({
        //scan option
        formats: "QR_CODE",
        resultDisplayDuration: 500,
        beepOnScan: true,
        message: "Press hardware back button to stop scanning",
        reportDuplicates: false, // count only unique scan, if staff is not sure, quit scan and rescan

        // this callback will be invoked for every unique scan in realtime!
        continuousScanCallback: function (result) {
            //count scanning
            scanCount++;
            //toast and vibrate for successful scan (no repeat)
            Toast.makeText(result.text).show();
            vibrator.vibrate(500);

            //QR result is username, id. Split them to array.
            let arr = result.text.split(",");
            //arr[0] is username, arr[1] is id
            // console.log("Splitted scan result: "+arr[0]+"/"+arr[1]);

            //get scan time in mysql format
            let date = new Date();
            //this is locale timestamp
            let timestamp = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' ');
            //this is ISO timestamp
            // let timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');

            //keep scan result in JSON array
            // arrScan.push({USER_ID:arr[1], ACTIVITY_ID:activityJSON['ID'], SCAN_BY_ID: userInfo['ID'], SCANNING_TIME:timestamp});
            arrScan.push(
                {USER_ID: arr[1].trim(), 
                    STUDENT_ID: arr[0].trim(),  //not for uploading, just only for checking later
                    ACTIVITY_ID: activityJSON[args.index].ID, 
                    SCAN_BY_ID: userInfo["ID"], 
                    SCANNING_TIME: timestamp}
            );

            //reach MAX scan
            if (scanCount == MAX_SCAN) {
                //stop scan and call closeCallback()
                barcodescanner.stop();
            }
        },
        closeCallback: function () {
            //This function is called when we stop scanner by code or by button
            returnFromScan = true;

            if(scanCount == 0) {
                //user cancel scan immediately, no scan data at all
                return;
            }

            //get activity id and gen key with it to save scan data
            var activityID = activityJSON[args.index].ID;
            var keyname = keyScanPrefix + activityID;
            //is there previous scan data?
            if(!appSettings.hasKey(keyname)) {
                //no previous data, save
                let arrScanString = JSON.stringify(arrScan);
                appSettings.setString(keyname, arrScanString);
                // console.log("Total scan = "+arrScan.length+ ", Value = "+arrScanString);       
                Toast.makeText("Scan saved").show();
                //refresh listview
                //connectDB("now");     //simplest, but need Internet
                refreshBadge();
            }
            else {
                //existing data, check repeated values and merge if required
                let arrScanOld = JSON.parse(appSettings.getString(keyname));
                let arrScanFinal = JSON.parse(appSettings.getString(keyname));

                //look for repeated value in previous scan
                //for each value of current scan
                for(var i=0; i<arrScan.length; i++) {
                    // let uid = arrScan[i].USER_ID;
                    let sid = arrScan[i].STUDENT_ID;
                    //for each value of previous scan
                    for(var j=0; j<arrScanOld.length; j++) {
                        //if id is repeated
                        if(sid == arrScanOld[j].STUDENT_ID) {
                            console.log("Repeat: " + sid);
                            break;
                        }
                    }
                    //not repeat
                    if(j == arrScanOld.length) {
                        //add this student
                        arrScanFinal.push(arrScan[i]);
                    }
                }
                //save to app setting if any new scan
                if(arrScanFinal.length > arrScanOld.length) {
                    let arrScanString = JSON.stringify(arrScanFinal);
                    appSettings.setString(keyname, arrScanString);
                    //console.log("Total scan = "+arrScanFinal.length+ ", Value = "+arrScanString);
                    Toast.makeText("New scan saved to "+keyname).show();
                    //refresh listview
                    //connectDB("now");
                    // console.log("after added scan, total scan = "+arrScanFinal.length);
                    refreshBadge();
                }
                else {
                    Toast.makeText("Scan repeated, not saved!").show();
                }
            }
        } //after this function, scan page closes and returns back to onload()      
    }).then(
        function () {
            //This function for single scan
            console.log("We're now reporting scan results in 'continuousScanCallback'");
        },
        function (error) {
            console.log("No scan: " + error);
            Toast.makeText("No scan: " + error).show();
        }
    );    
}

//========================= For emulator: Bulk Scan QR codes ===============================
/*
exports.scanQR2 = function(args) {
    //if NOT today activity, can't scan
    if(!canScan) {
        console.log("Cannot scan, not today activity");
        return;
    }

    let date = new Date();
    //this is locale timestamp
    let timestamp = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString().slice(0, 19).replace('T', ' ');
    var arrScan = [];

    //dummy
    arrScan.push(
        {USER_ID: 21, 
            STUDENT_ID: "6131501001",
            ACTIVITY_ID: activityJSON[args.index].ID, 
            SCAN_BY_ID: userInfo["ID"], 
            SCANNING_TIME: timestamp}
    );

    arrScan.push(
        {USER_ID: 20, 
            STUDENT_ID: "6131305001",
            ACTIVITY_ID: activityJSON[args.index].ID, 
            SCAN_BY_ID: userInfo["ID"], 
            SCANNING_TIME: timestamp}
    );

    let activityID = activityJSON[args.index].ID;
    // console.log(activityID);
    let keyname = keyScanPrefix + activityID;

    let arrScanString = JSON.stringify(arrScan);
    appSettings.setString(keyname, arrScanString);
    console.log("Total scan = "+arrScan.length+ ", Value = "+arrScanString);       
    Toast.makeText("Scan saved").show();
    //reload page, delete this
    connectDB("now");
}
*/

//========================= Upload Scan Result ===============================
exports.uploadQR = function(args) {
    //if there is 0 scan, don't upload
    let keyname = keyScanPrefix + activityJSON[args.index].ID;
    if(!appSettings.hasKey(keyname)) {
        Toast.makeText("No scan data for this activity, key = "+keyname).show();
        return;
    }

    //confirm dialog
    const options = {
        title: "Manage Scan Data",
        message: "What to do?",
        okButtonText: "Upload",
        cancelButtonText: "Cancel",
        neutralButtonText: "View"
    };

    dialogs.confirm(options).then(function(result) {
        //result could be true/false/undefined             
        if(result==true) {
            //get a scanned JSON string from the selected activity    
            let scanData = appSettings.getString(keyname);

            //upload
            loader.show();
            http.request({
                url: urlUploadQR,
                method: "POST",
                timeout: 5000,
                content: "scanString=" + scanData
            }).then(function (response) {
                //hide activity indicator
                loader.hide();

                const statusCode = response.statusCode;
                //convert response to JSON array
                let responseJSON = response.content.toJSON();
                alert(responseJSON["status"]);
                
                //if insert OK or duplicated data
                if(statusCode == 200 || statusCode == 401) {
                    //remove saved scan data
                    appSettings.remove(keyname);
                    // Toast.makeText("Clear key "+keyname).show();
                    //refresh page
                    refreshBadge();
                }        
            }, function (e) {
                //connection failed or other errors
                loader.hide();
                alert("Error, please check your Internet connection");
                console.log("Error" + e);
            });
        }
        else if(result == undefined) {
            //show data in key
            let scanData = JSON.parse(appSettings.getString(keyname));
            let temp = "";
            for(let k=0; k<scanData.length; k++) {
                temp += (scanData[k].STUDENT_ID + "\n");
            }
            alert(temp);
        }
    });//end dialog    
}

//--------------------- Refresh Scan Badge ---------------------------------
function refreshBadge() {
    // console.log("Total badge refreshed = "+activityJSON.length);
    //This is offline refresh, not most effective but OK
    for(var i=0; i<activityJSON.length; i++) {
        let atvid = activityJSON[i].ID;
        let keyname = keyScanPrefix + atvid;

        if(appSettings.hasKey(keyname)) {
            let temp = JSON.parse(appSettings.getString(keyname));
            activityJSON[i]['BADGE'] = temp.length;
            // console.log(temp.length);
        }
        else {
            activityJSON[i]['BADGE'] = 0;
        }
    }

    //show activities in listview
    activityArray.length = 0;
    activityArray.push(activityJSON);
    //usually it should be auto refresh with the command below, but we found no refresh in some cases
    //so we force refresh one more time
    lvActivity.refresh();
}