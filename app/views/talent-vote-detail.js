const frameModule = require("ui/frame");
const http = require("http");
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
const appSettings = require("application-settings");
const dialogs = require("ui/dialogs");
const mainURL = require("./mainURL");

var imgPhoto, lblTeamNumber, lblTeamTitle, lblPerformance;
var teamJSON = null;

const urlVoteTalent = mainURL.getURL("voteTalent");
const imgDir = mainURL.getURL("imageDir");
const loader = new LoadingIndicator();

exports.onLoaded = function(args) {
    var page = args.object;
    imgPhoto = page.getViewById("imgPhoto");
    lblTeamNumber = page.getViewById("lblTeamNumber");
    lblTeamTitle = page.getViewById("lblTeamTitle");
    lblPerformance = page.getViewById("lblPerformance");
}

exports.onNavigatedTo = function(args) {
    const data = args.object.navigationContext;

    teamJSON = data.teamInfo;
    imgPhoto.src = imgDir + teamJSON["VOTE_PICTURE"];
    lblTeamNumber.text = teamJSON["TEAM_NUMBER"];
    lblTeamTitle.text = teamJSON["TEAM_TITLE"];
    lblPerformance.text = teamJSON["PERFORMANCE_TITLE"];
}

exports.vote = function() {
    //confirm submit
    const options = {
        title: "Confirm",
        message: "Vote for " + teamJSON["TEAM_NUMBER"] + " ?",
        okButtonText: "Vote",
        cancelButtonText: "Cancel"
    };
    dialogs.confirm(options).then(function(result) {
        // result can be true/false/undefined
        if(result) {
            // console.log("You vote");
            submitVote();
        }
    });    
    //do not place any code below here, otherwise it will be called before getting dialog answer
}

function submitVote() {
    const userInfo = JSON.parse(appSettings.getString("kUserInfo"));
    const userID = userInfo["ID"];

    loader.show();
    http.request({
        url: urlVoteTalent,
        method: "PUT",
        content: "teamID=" + teamJSON["ID"] + "&" + "userID=" + userID,
    }).then(function (response) {
        loader.hide();
        const result = response.content.toJSON();
        alert(result['status']);
        
        //jump to menu without history
        frameModule.topmost().navigate({
            moduleName: "views/menu",
            clearHistory: true
        });

    }, function (e) {
        loader.hide();
        alert("Error, please check your Internet connection");
    });
}

exports.back = function () {
    frameModule.topmost().goBack();
}