const platform = require("platform");
const local = true;     //flag to select localhost
var host, path, service;

//=================== Server References =================
if(local) {
    //localhost
    if(platform.isIOS) {
        host = "http://localhost";
        
    }
    else if(platform.isAndroid) {
        host = "http://10.0.3.2";
    }
    path = "/mfha";
    service = "/mobileservice.php";
}
else {
    //URL for mfes dev server
    host = "http://dev-mfha.mfessolutions.com";
    path = "/apiMobile/v1";
    service = "/mobileservice.php";
}

//============================= Path Settings =================================
const urlImgDir = host + path + "/img/";
const urlDoc = host + path + "/doc/";

const urlLogin = host + path + service + "/login";
const urlNews = host + path + service + "/news";
const urlActivity = host + path + service + "/activity_user";
const urlActivityYear = host + path + service + "/activity_year";
const urlActivityNow = host + path + service + "/activity_now";
const urlHandbook = host + path + service + "/handbook";
const urlContact = host + path + service + "/contact";
const urlRegister = host + path + service + "/canRegister";
const urlVote = host + path + service + "/canVote";
const urlUploadImage = host + path + service + "/uploadImageTalent";
const urlRegisterTalent = host + path + service + "/registerTalent";
const urlTalentTeam = host + path + service + "/talent_team";
const urlVoteTalent = host + path + service + "/voteTalent";
const urlSurveyQuestion = host + path + service + "/survey_question";
const urlSurveyAnswer = host + path + service + "/survey_answer";
const urlUpdateSurveyStatus = host + path + service + "/update_activity_status";
const urlUserTalent = host + path + service + "/canRegister";
const urlUploadQR = host + path + service + "/uploadQR";

//============================= Local Path Settings =================================
const appFolder = "HLL";
const photoTalent = "talent.png";

//============================= Functions =================================
exports.getURL = function(type) {
    // switch(type) {
    //     case "login":
    //         return urlLogin;
    //         break;
    //     case "news":
    //         return urlNews;
    //         break;
    //     case "imageDir":
    //         return urlImgDir;
    //         break;
    //     case "activity":
    //         return urlActivity;
    //         break;
    //     case "activity_year":
    //         return urlActivityYear;
    //         break;
    // }

    if(type=="login") {
        return urlLogin;
    }
    else if(type=="news") {
        return urlNews;
    }
    else if(type=="imageDir") {
        return urlImgDir;
    }
    else if(type=="activity") {
        return urlActivity;
    }
    else if(type=="activity_year") {
        return urlActivityYear;
    }
    else if(type=="activity_now") {
        return urlActivityNow;
    }
    else if(type=="handbook") {
        return urlHandbook;
    }
    else if(type=="doc") {
        return urlDoc;
    }
    else if(type=="contact") {
        return urlContact;
    }
    else if(type=="register") {
        return urlRegister;
    }
    else if(type=="vote") {
        return urlVote;
    }
    else if(type=="uploadImage") {
        return urlUploadImage;
    }
    else if(type=="registerTalent") {
        return urlRegisterTalent;
    }
    else if(type=="talentTeam") {
        return urlTalentTeam;
    }
    else if(type=="voteTalent") {
        return urlVoteTalent;
    }
    else if(type=="surveyQuestion") {
        return urlSurveyQuestion;
    }
    else if(type=="surveyAnswer") {
        return urlSurveyAnswer;
    }
    else if(type=="updateSurveyStatus") {
        return urlUpdateSurveyStatus;
    }
    else if(type=="userTalent") {
        return urlUserTalent;
    }
    else if(type=="uploadQR") {
        return urlUploadQR;
    }
    else if(type=="appFolder") {
        return appFolder;
    }
    else if(type=="photoTalent") {
        return photoTalent;
    }
}